﻿using System;
using System.IO;
using AmiGo.Droid.Services;
using AmiGo.Services;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper))]
namespace AmiGo.Droid.Services
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);
        }
    }
}