﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmiGo.CustomRenderes;
using AmiGo.Droid.CustomRenderes;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(EntryRendererAndroid))]
namespace AmiGo.Droid.CustomRenderes
{
    public class EntryRendererAndroid : EntryRenderer
    {
        //public EntryRendererAndroid(Context context) : base(context)
        //{
        //}

        public EntryRendererAndroid(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);


            if (Control != null)
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(Android.Graphics.Color.Transparent);
                else
                    Control.Background.SetColorFilter(Android.Graphics.Color.Transparent, PorterDuff.Mode.Clear);
            }
        }        
    }
}