﻿using System;
using System.IO;
using AmiGo.iOS.Services;
using AmiGo.Services;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper))]
namespace AmiGo.iOS.Services
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, filename);
        }
    }
}