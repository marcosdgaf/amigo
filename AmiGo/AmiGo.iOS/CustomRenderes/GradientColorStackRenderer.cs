﻿using AmiGo.CustomRenderes;
using AmiGo.iOS.CustomRenderes;
using CoreAnimation;
using CoreGraphics;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(GradientColorStack), typeof(GradientColorStackRenderer))]
namespace AmiGo.iOS.CustomRenderes
{
    public class GradientColorStackRenderer : VisualElementRenderer<StackLayout>
    {
        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            GradientColorStack stack = (GradientColorStack)this.Element;
            CGColor startColor = stack.StartColor.ToCGColor();

            CGColor endColor = stack.EndColor.ToCGColor();

            #region for Vertical Gradient
            if (stack.Vertical == true)
            {
                var gradientLayer = new CAGradientLayer();
                gradientLayer.Frame = rect;
                gradientLayer.Colors = new CGColor[] { startColor, endColor };
                NativeView.Layer.InsertSublayer(gradientLayer, 0);
            }
            #endregion
            #region for Horizontal Gradient
            else
            {

                var gradientLayer = new CAGradientLayer()
                {
                  StartPoint = new CGPoint(0, 0.5),
                  EndPoint = new CGPoint(1, 0.5)
                };
                gradientLayer.Frame = rect;
                gradientLayer.Colors = new CGColor[] { startColor, endColor };
                NativeView.Layer.InsertSublayer(gradientLayer, 0);                
            }
            #endregion



        }
    }
}