﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AmiGo.CustomRenderes;
using AmiGo.iOS.CustomRenderes;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(EntryRendererIOS))]
namespace AmiGo.iOS.CustomRenderes
{
    public class EntryRendererIOS: EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                // do whatever you want to the UITextField here!
                //Control.BackgroundColor = UIColor.FromRGB(204, 153, 255);
                Control.BorderStyle = UITextBorderStyle.None;
                
            }
        }
    }
}