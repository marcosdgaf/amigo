﻿using Acr.UserDialogs;
using AmiGo.Helpers;
using AmiGo.Models;
using AmiGo.Services;
using AmiGo.Views;
using Microsoft.AppCenter.Crashes;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Plugin.Connectivity;

namespace AmiGo.ViewModels
{
    public class SelectGrupoViewModel
    {
        public Grupo Grupos { get; set; }
        public bool Selected { get; set; }
    }

    public class PedidoViewModel : BaseViewModel
    {
        public ObservableCollection<Grouping<SelectGrupoViewModel, Produto>> GruposCardapio { get; set; }

        public Command<Grouping<SelectGrupoViewModel, Produto>> HeaderSelectedCardapioCommand
        {
            get
            {
                return new Command<Grouping<SelectGrupoViewModel, Produto>>(g =>
                {
                    if (g == null) return;
                    g.Key.Selected = !g.Key.Selected;
                    if (g.Key.Selected)
                    {
                        Cardapio.Where(i => (i.Grupo.Codigo == g.Key.Grupos.Codigo)).ForEach(g.Add);
                    }
                    else
                    {
                        g.Clear();
                    }
                });
            }
        }

        public ObservableCollection<Grouping<SelectGrupoViewModel, Produto>> GruposBebidas { get; set; }

        public Command<Grouping<SelectGrupoViewModel, Produto>> HeaderSelectedBebidasCommand
        {
            get
            {
                return new Command<Grouping<SelectGrupoViewModel, Produto>>(g =>
                {
                    if (g == null) return;
                    g.Key.Selected = !g.Key.Selected;
                    if (g.Key.Selected)
                    {
                        Bebidas.Where(i => (i.Grupo.Codigo == g.Key.Grupos.Codigo))
                            .ForEach(g.Add);
                    }
                    else
                    {
                        g.Clear();
                    }
                });
            }
        }


        #region Commands

        public Command AumentarQtdeCommand { get; }
        public Command DiminuirQtdeCommand { get; }
        public Command CancelaCommand { get; }
        public Command ConfirmarPedidoCommand { get; }
        public Command AdicionaisCommand { get; }
        public Command AumentarQtdeConsumoCommand { get; }
        public Command DiminuirQtdeConsumoCommand { get; }
        public Command FinalizarComandaCommand { get; }
        public Command AumentarDiminuirPessoasCommand { get; }
        public Command AumentarDiminuirServicoCommand { get; }
        public Command ImprimirComandaCommand { get; }
        public Command CancelarItemCommand { get; }

        #endregion

        #region Colecoes       


        private ObservableCollection<Produto> _cardapio;
        public ObservableCollection<Produto> Cardapio
        {
            get { return _cardapio; }
            set
            {
                SetProperty(ref _cardapio, value);
                OnPropertyChange(nameof(Cardapio));
            }
        }

        private ObservableCollection<Mesa> _mesas;
        public ObservableCollection<Mesa> Mesas
        {
            get { return _mesas; }
            set
            {
                SetProperty(ref _mesas, value);
                OnPropertyChange(nameof(Mesas));
            }
        }

        private ObservableCollection<Produto> _bebidas;
        public ObservableCollection<Produto> Bebidas
        {
            get { return _bebidas; }
            set
            {
                SetProperty(ref _bebidas, value);
                OnPropertyChange(nameof(Bebidas));
            }
        }

        private ObservableCollection<Produto> _produtosCheckout;
        public ObservableCollection<Produto> ProdutosCheckout
        {
            get { return _produtosCheckout; }
            set
            {
                SetProperty(ref _produtosCheckout, value);
                OnPropertyChange(nameof(ProdutosCheckout));
            }
        }

        private ObservableCollection<PedidoItem> _itensPedidos;
        public ObservableCollection<PedidoItem> ItensPedidos
        {
            get { return _itensPedidos; }
            set
            {
                SetProperty(ref _itensPedidos, value);
                OnPropertyChange(nameof(ItensPedidos));
            }
        }

        private ObservableCollection<PedidoItem> _pedidoItens;
        public ObservableCollection<PedidoItem> PedidoItens
        {
            get { return _pedidoItens; }
            set
            {
                SetProperty(ref _pedidoItens, value);
                OnPropertyChange(nameof(PedidoItens));
            }
        }

        private ObservableCollection<Produto> _consumacoes;
        public ObservableCollection<Produto> Consumacoes
        {
            get { return _consumacoes; }
            set
            {
                SetProperty(ref _consumacoes, value);
                OnPropertyChange(nameof(Consumacoes));
            }
        }

        private ObservableCollection<Produto> _listaProdutos;
        public ObservableCollection<Produto> ListaProdutos
        {
            get { return _listaProdutos; }
            set
            {
                SetProperty(ref _listaProdutos, value);
                OnPropertyChange(nameof(ListaProdutos));
            }
        }

        #endregion


        public PedidoItem PedidoItem { get; set; }
        private string _pesquisa;
        public string Pesquisa
        {
            get { return _pesquisa; }
            set
            {
                SetProperty(ref _pesquisa, value);
                OnPropertyChange(nameof(Pesquisa));
            }
        }

        private int _pessoas;
        public int Pessoas
        {
            get { return _pessoas; }
            set
            {
                SetProperty(ref _pessoas, value);
                OnPropertyChange(nameof(Pessoas));
            }
        }

        private decimal _servico;
        public decimal Servico
        {
            get { return _servico; }
            set
            {
                if (SetProperty(ref _servico, value))
                    RecalculaServico();
                OnPropertyChange(nameof(Servico));
            }
        }

        private decimal _totalConsumo;
        public decimal TotalConsumo
        {
            get { return _totalConsumo; }
            set
            {
                SetProperty(ref _totalConsumo, value);
                OnPropertyChange(nameof(TotalConsumo));
            }
        }


        private decimal _totalServico;
        public decimal TotalServico
        {
            get { return _totalServico; }
            set
            {
                SetProperty(ref _totalServico, value);
                OnPropertyChange(nameof(TotalServico));
            }
        }

        private decimal _totalMesa;
        public decimal TotalMesa
        {
            get { return _totalMesa; }
            set
            {
                SetProperty(ref _totalMesa, value);
                OnPropertyChange(nameof(TotalMesa));
            }
        }

        public string Mesa { get; set; }

        public string Ficha { get; set; }

        public string Nome { get; set; }

        public bool IsBusy { get; set; }

        public PedidoViewModel(string mesa, string ficha, string nome)
        {
            TotalConsumo = 0;
            TotalServico = 0;
            TotalMesa = 0;
            Pessoas = 1;
            Servico = App.Parametro.MinGarcom;
            Mesa = mesa;
            Ficha = ficha;
            Nome = nome;
            Mesas = new ObservableCollection<Mesa>();
            Cardapio = new ObservableCollection<Produto>();
            GruposCardapio = new ObservableCollection<Grouping<SelectGrupoViewModel, Produto>>();

            Bebidas = new ObservableCollection<Produto>();
            GruposBebidas = new ObservableCollection<Grouping<SelectGrupoViewModel, Produto>>();

            ProdutosCheckout = new ObservableCollection<Produto>();

            ItensPedidos = new ObservableCollection<PedidoItem>();

            PedidoItens = new ObservableCollection<PedidoItem>();

            Consumacoes = new ObservableCollection<Produto>();

            ListaProdutos = new ObservableCollection<Produto>();

            CancelaCommand = new Command(ExecuteCancelaCommand);
            ConfirmarPedidoCommand = new Command(ExecuteConfirmarPedidoCommand);
            AumentarQtdeCommand = new Command<Produto>(ExecuteAumentarQtdeCommand);
            DiminuirQtdeCommand = new Command<Produto>(ExecuteDiminuirQtdeCommand);
            AdicionaisCommand = new Command<Produto>(ExecuteAdicionaisCommand);
            AumentarQtdeConsumoCommand = new Command<Produto>(ExecuteAumentarQtdeConsumoCommand);
            DiminuirQtdeConsumoCommand = new Command<Produto>(ExecuteDiminuirQtdeConsumoCommand);
            FinalizarComandaCommand = new Command(ExecuteFinalizarComandaCommand);
            AumentarDiminuirPessoasCommand = new Command<string>(ExecuteAumentarDiminuirPessoasCommand);
            AumentarDiminuirServicoCommand = new Command<string>(ExecuteAumentarDiminuirServicoCommand);
            ImprimirComandaCommand = new Command(ExecuteImprimirComandaCommand);
            CancelarItemCommand = new Command<Produto>(ExecuteCancelarItemCommand);


            VerificaConsumacao(Mesa, Ficha);

            if (App.Parametro.PesquisaDescricao == true)
            {
                Task.Run(async () =>
                {
                    await CarregaItens();
                });
            }
            else
            {
                CarregaCardapio();
                CarregaBebidas();
            }
        }

        private async void ExecuteImprimirComandaCommand()
        {
            var answer = await UserDialogs.Instance.ConfirmAsync("Deseja Imprimir a Comanda?", null, "Sim", "Não");
            if (answer)
            {
                if (Consumacoes.Count == 0)
                {
                    await App.Current.MainPage.DisplayAlert("AmiGô: ", "Consumo não encontrado na comanda ", "OK");
                    return;
                }
                var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                var result = await amiGoApiService.PostPrinterFichaAsync(new Ficha { Mesa = Convert.ToInt32(Mesa), Numero = Convert.ToInt32(Ficha) });
            }
        }

        private void ExecuteAumentarDiminuirServicoCommand(string Tipo)
        {
            try
            {
                if (Tipo == "0")
                {
                    if (Servico != App.Parametro.MinGarcom && Servico != 0)
                        Servico--;
                    else if (Servico == App.Parametro.MinGarcom)
                        Servico = 0;

                }
                else
                {
                    if (Servico == 0)
                        Servico = App.Parametro.MinGarcom;
                    else if (Servico < App.Parametro.MaxGarcom)
                        Servico++;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public void RecalculaServico()
        {
            if (TotalConsumo > 0)
                TotalServico = (TotalConsumo / 100) * Servico;
            TotalMesa = TotalConsumo + TotalServico;
        }

        private void ExecuteAumentarDiminuirPessoasCommand(string Tipo)
        {
            try
            {
                if (Tipo == "0")
                {
                    if (Pessoas != 1)
                        Pessoas--;
                }
                else
                {
                    Pessoas++;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public async Task CarregaItens()
        {
            try
            {
                Bebidas.Clear();
                Cardapio.Clear();

                var bebidas = await App.Database.GetProdutosAsync(1);
                if (bebidas != null)
                {
                    foreach (var bebida in bebidas)
                    {
                        bebida.VisualizarPreco = App.Parametro.MostrarPrecoCardapio == "S";
                        Bebidas.Add(bebida);
                        OnPropertyChange(nameof(Bebidas));
                    }
                }

                var ItensCardapio = await App.Database.GetProdutosAsync(2);
                if (ItensCardapio != null)
                {
                    foreach (var item in ItensCardapio)
                    {
                        item.VisualizarPreco = App.Parametro.MostrarPrecoCardapio == "S";
                        Cardapio.Add(item);
                        OnPropertyChange(nameof(Cardapio));
                    }
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteFinalizarComandaCommand()
        {
            try
            {

                if (App.Parametro.BloquearFechamento == "S")
                {
                    await App.Current.MainPage.DisplayAlert("AmiGô: ", "Sem permissão para finalizar a comanda ", "OK");
                    return;
                }

                if (Consumacoes.Count == 0)
                    return;

                if (!IsBusy)
                {
                    IsBusy = true;
                    var page = new FinalizadoresPage(Mesa, Ficha, Nome);
                    await PopupNavigation.Instance.PushAsync(page);
                    IsBusy = false;
                }

                //var answer = await UserDialogs.Instance.ConfirmAsync("Confirma Finalização?", null, "Sim", "Não");
                //if (answer)
                //{
                //    var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                //    //var result = await amiGoApiService.PutFechamentoAsync(Mesa, Ficha, Servico, Pessoas);

                //    var result = await amiGoApiService.PutFechamentoAsync(new PreVenda
                //    {
                //        Ficha = Convert.ToInt32(Ficha),
                //        Mesa = Convert.ToInt32(Mesa),
                //        PorcentagemServico = Convert.ToDecimal(Servico),
                //        QtdePessoas = Pessoas,
                //        Usuario = App.Usuario.Id,
                //        Vendedor = App.Usuario.Vendedor,
                //        Maquina = Convert.ToInt32(App.Config.Device)
                //    }
                //    );

                //    if (result.Sucesso)
                //    {
                //        await App.Current.MainPage.DisplayAlert("", result.Mensagem, "OK");
                //        //VerificaConsumacao(Mesa, Ficha);
                //        await App.Current.MainPage.Navigation.PopPopupAsync();
                //    }
                //    else
                //    {
                //        await App.Current.MainPage.DisplayAlert("Erro ao gerar a Pré-venda: ", result.Mensagem, "OK");
                //        //Crashes. (result.Mensagem);
                //    }

                //}
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public async void VerificaConsumacao(string mesa, string ficha)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mesa) == false)
                {

                    if (!await CrossConnectivity.Current.IsRemoteReachable(App.BaseUrl.ToUpper().Replace("HTTP://", "").Replace(":2390/API/", ""), 2390))
                    {
                        await Task.Delay(2000);
                        if (!await CrossConnectivity.Current.IsRemoteReachable(App.BaseUrl.ToUpper().Replace("HTTP://", "").Replace(":2390/API/", ""), 2390))
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Verifique Sua Conexão", "OK");
                            IsBusy = false;
                            return;
                        }
                    }

                    var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                    var result = await amiGoApiService.GetConsumacaoAsync(mesa, ficha);

                    Consumacoes.Clear();
                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            var grupo = await App.Database.GetGrupoAsync();
                            var produto = await App.Database.GetProdutoAsync(item.Produto);
                            if (produto != null)
                            {
                                var buscaGrupo = grupo.Where(g => g.Codigo == produto.CodGrupo && g.Secao == produto.CodSecao).FirstOrDefault();
                                if (buscaGrupo != null && buscaGrupo.Codigo > 0)
                                {
                                    produto.Grupo = grupo.Where(g => g.Codigo == produto.CodGrupo && g.Secao == produto.CodSecao).FirstOrDefault();
                                    produto.Quantidade = item.Quantidade;
                                    produto.Consumido = item.Quantidade;
                                    produto.Hora = item.Hora;
                                    Consumacoes.Add(produto);
                                }
                            }
                        }

                        TotalConsumo = result.Sum(c => c.Valor);
                        RecalculaServico();
                    }


                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Não foi possivel Verificar a consumação", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteAdicionaisCommand(Produto produto)
        {
            if (!IsBusy)
            {
                IsBusy = true;

                ItensPedidos.Clear();

                var itens = PedidoItens.Where(p => p.Produto == produto.Codigo).OrderByDescending(x => x.Data);

                itens.ForEach(sc => ItensPedidos.Add(sc));

                if (ItensPedidos.Count > 0)
                {
                    var page = new AdicionaisPage(ItensPedidos);
                    await PopupNavigation.Instance.PushAsync(page);
                }
                IsBusy = false;
            }
        }

        public async void CarregaBebidas()
        {
            Bebidas.Clear();

            var grupo = await App.Database.GetGrupoAsync();
            var bebidas = await App.Database.GetProdutosAsync(1);
            if (bebidas != null)
            {
                foreach (var bebida in bebidas)
                {
                    if (bebida.CodGrupo > 0 && bebida.CodSecao > 0 && bebida.Valor > 0)
                    {
                        var buscaGrupo = grupo.Where(g => g.Codigo == bebida.CodGrupo && g.Secao == bebida.CodSecao).FirstOrDefault();
                        if (buscaGrupo != null && buscaGrupo.Codigo > 0)
                        {
                            bebida.Descricao = Convert.ToString(bebida.Codigo) + " - " + bebida.Descricao;
                            bebida.Grupo = grupo.Where(g => g.Codigo == bebida.CodGrupo && g.Secao == bebida.CodSecao).FirstOrDefault();
                            bebida.VisualizarPreco = App.Parametro.MostrarPrecoCardapio == "S";
                            Bebidas.Add(bebida);
                        }
                    }
                }
            }

            OnPropertyChange(nameof(Bebidas));
            var grupos = Bebidas.Select(x => new SelectGrupoViewModel { Grupos = x.Grupo, Selected = false })
                    .OrderBy(o => o.Grupos.Descricao)
                    .GroupBy(sc => new { sc.Grupos.Codigo })
                    .Select(g => g.First())
                    .ToList();
            grupos.ForEach(sc => GruposBebidas.Add(new Grouping<SelectGrupoViewModel, Produto>(sc, new List<Produto>())));
            OnPropertyChange(nameof(GruposBebidas));


        }

        public async void CarregaCardapio()
        {
            Cardapio.Clear();
            var grupo = await App.Database.GetGrupoAsync();
            var ItensCardapio = await App.Database.GetProdutosAsync(2);

            if (ItensCardapio != null)
            {
                foreach (var item in ItensCardapio)
                {
                    if (item.CodGrupo > 0 && item.CodSecao > 0 && item.Valor > 0)
                    {
                        var buscaGrupo = grupo.Where(g => g.Codigo == item.CodGrupo && g.Secao == item.CodSecao).FirstOrDefault();
                        if (buscaGrupo != null && buscaGrupo.Codigo > 0)
                        {
                            item.Descricao = Convert.ToString(item.Codigo) + " - " + item.Descricao;
                            item.Grupo = grupo.Where(g => g.Codigo == item.CodGrupo && g.Secao == item.CodSecao).FirstOrDefault();
                            item.VisualizarPreco = App.Parametro.MostrarPrecoCardapio == "S";
                            Cardapio.Add(item);
                        }
                    }
                }
            }

            OnPropertyChange(nameof(Cardapio));

            //Consulta.Linha = (Linha == null ? 0 : Linha.Codigo);
            var grupos = Cardapio.Select(x => new SelectGrupoViewModel { Grupos = x.Grupo, Selected = false })
                    .OrderBy(o => o.Grupos.Descricao)
                    .GroupBy(sc => new { sc.Grupos.Codigo })
                    .Select(g => g.First())
                    .ToList();
            grupos.ForEach(sc => GruposCardapio.Add(new Grouping<SelectGrupoViewModel, Produto>(sc, new List<Produto>())));
            OnPropertyChange(nameof(GruposCardapio));
        }

        public void CarregaItensPedidos()
        {
            ProdutosCheckout.Clear();


            var itensCardapio = Cardapio.Where(x => x.Quantidade > 0).ToList();

            foreach (var produtoCardapio in itensCardapio)
            {
                ProdutosCheckout.Add(produtoCardapio);
            }

            var itensBebidas = Bebidas.Where(x => x.Quantidade > 0).ToList();

            foreach (var produtoBebida in itensBebidas)
            {
                ProdutosCheckout.Add(produtoBebida);
            }

            OnPropertyChange(nameof(ProdutosCheckout));

        }

        private async void ExecuteConfirmarPedidoCommand()
        {
            try
            {

                if (!IsBusy)
                {
                    IsBusy = true;

                    using (var objDialog = UserDialogs.Instance.Loading("Enviando Pedido", null, null, true, MaskType.Black))
                    {
                        if (PedidoItens.Count > 0)
                        {
                            if (!await CrossConnectivity.Current.IsRemoteReachable(App.BaseUrl.ToUpper().Replace("HTTP://", "").Replace(":2390/API/", ""), 2390))
                            {
                                await Task.Delay(2000);
                                if (!await CrossConnectivity.Current.IsRemoteReachable(App.BaseUrl.ToUpper().Replace("HTTP://", "").Replace(":2390/API/", ""), 2390))
                                {
                                    await App.Current.MainPage.DisplayAlert("AmiGo", "Verifique Sua Conexão", "OK");
                                    IsBusy = false;
                                    return;
                                }
                            }
                            foreach (var item in PedidoItens)
                            {


                                if (!string.IsNullOrEmpty(item.Observacao))
                                    item.Observacao = item.Observacao + ";";

                                foreach (var obs in item.ProdutoObservacao)
                                {
                                    if (obs.Quantidade > 0)
                                    {
                                        if (obs.Quantidade > 1)
                                            item.Observacao = item.Observacao + obs.Quantidade + " " + obs.Observacao + "; ";
                                        else
                                            item.Observacao = item.Observacao + " " + obs.Observacao + "; ";
                                    }

                                }
                                var pizzasSabores = item.PizzasSabores.Where(p => p.Selecionado).ToList();

                                if (pizzasSabores.Count > 0)
                                {
                                    item.SaboresPizza = pizzasSabores.Count;
                                    item.Observacao = item.Observacao + "Demais Sabores: ;";
                                    foreach (var pizza in pizzasSabores)
                                    {
                                        item.Observacao = item.Observacao + pizza.Descricao + ";";

                                        if (pizza.Valor > item.Valor)
                                            item.Valor = pizza.Valor;
                                    }
                                }


                                var adicionais = new ObservableCollection<Adicional>();
                                foreach (var adi in item.Adicionais)
                                {
                                    if (adi.Quantidade > 0)
                                        adicionais.Add(adi);
                                }

                                item.Adicionais.Clear();

                                foreach (var adicional in adicionais)
                                {
                                    item.Adicionais.Add(adicional);
                                }

                            }

                            #region Agrupa Itens
                            //Itens sem Observação e sem Adicional
                            //var itensAgrupaveis = PedidoItens.Where(p => p.Observacao == null && p.Adicionais.Count == 0);
                            var itensAgrupaveis = (from i in PedidoItens.Where(p => p.Observacao == null && p.Adicionais.Count == 0)
                                                   group i by new { i.Produto, i.Maquina, i.Vendedor, i.PorGarcom, i.CodGrupo, i.CodSecao, i.Descricao, i.Ficha, i.Mesa, i.Nome, i.Valor, i.Quantidade }
                                                  into cons
                                                   select new PedidoItem
                                                   {
                                                       Produto = cons.Key.Produto,
                                                       Quantidade = cons.Sum(q => q.Quantidade),
                                                       Valor = cons.Sum(q => q.Valor),
                                                       Descricao = cons.Key.Descricao,
                                                       PorGarcom = cons.Key.PorGarcom,
                                                       CodGrupo = cons.Key.CodGrupo,
                                                       CodSecao = cons.Key.CodSecao,
                                                       Ficha = cons.Key.Ficha,
                                                       Nome = cons.Key.Nome,
                                                       Mesa = cons.Key.Mesa,
                                                       Vendedor = cons.Key.Vendedor,
                                                       Maquina = cons.Key.Maquina
                                                   }).ToList();

                            var removeItensAgrupados = PedidoItens.Where(p => p.Observacao == null && p.Adicionais.Count == 0).ToList();

                            foreach (var item in removeItensAgrupados)
                            {
                                PedidoItens.Remove(item);
                            }

                            foreach (var item in itensAgrupaveis)
                            {
                                PedidoItens.Add(item);
                            }                        

                            #endregion

                            var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                            var result = await amiGoApiService.PostPedidoAsync(PedidoItens);

                            if (result == true)
                            {
                                await App.Current.MainPage.Navigation.PopPopupAsync();
                            }
                            else
                            {
                                await App.Current.MainPage.DisplayAlert("AmiGo", "Não foi possivel enviar o pedido", "OK");
                            }
                        }
                    }
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }

        }

        private async void ExecuteCancelaCommand()
        {
            await App.Current.MainPage.Navigation.PopPopupAsync();
        }

        private void ExecuteDiminuirQtdeCommand(Produto produto)
        {
            try
            {
                if ((produto != null && produto.Quantidade > 0) && ((produto.Quantidade - produto.Consumido) > 0))
                {
                    produto.Quantidade = produto.Quantidade - 1;

                    var iten = PedidoItens.Where(x => x.Produto == produto.Codigo).OrderByDescending(d => d.Data).First();

                    PedidoItens.Remove(iten);

                    var verificaListaConsumo = Consumacoes.Where(c => c.Codigo == produto.Codigo).FirstOrDefault();
                    if (verificaListaConsumo != null)
                        verificaListaConsumo.Quantidade = verificaListaConsumo.Quantidade - 1;


                    OnPropertyChange(nameof(produto));
                    OnPropertyChange(nameof(Cardapio));
                }
            }
            catch (Exception e)
            {
                App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private void ExecuteAumentarQtdeCommand(Produto produto)
        {
            try
            {
                if (produto != null)
                {
                    ItensPedidos.Clear();
                    produto.Quantidade = produto.Quantidade + 1;
                    PedidoItem = new PedidoItem { Valor = produto.Valor, Mesa = Mesa, Ficha = Ficha, Nome = Nome, Quantidade = 1, Produto = produto.Codigo, CodGrupo = produto.CodGrupo, CodSecao = produto.CodSecao, Descricao = produto.Descricao, Vendedor = App.Usuario.Vendedor, Maquina = Convert.ToInt32(App.Config.Maquina), QtdSaboresPizza = produto.QtdeSaboresPizza };
                    PedidoItens.Add(PedidoItem);
                    PedidoItem.Item = PedidoItens.Count();

                    var itens = PedidoItens.Where(p => p.Produto == produto.Codigo).OrderByDescending(x => x.Data);

                    itens.ForEach(sc => ItensPedidos.Add(sc));

                    if (produto.AbrirAdicionais == "S")
                    {
                        AbrirAdicionais(ItensPedidos);
                    }

                    var verificaListaConsumo = Consumacoes.Where(c => c.Codigo == produto.Codigo).FirstOrDefault();

                    if (verificaListaConsumo != null)
                        verificaListaConsumo.Quantidade = verificaListaConsumo.Quantidade + 1;

                }

                OnPropertyChange(nameof(produto));
                OnPropertyChange(nameof(Cardapio));
            }
            catch (Exception e)
            {
                App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private void ExecuteDiminuirQtdeConsumoCommand(Produto produto)
        {
            try
            {
                if ((produto != null && produto.Quantidade > 0) && ((produto.Quantidade - produto.Consumido) > 0))
                {
                    produto.Quantidade = produto.Quantidade - 1;

                    var item = PedidoItens.Where(x => x.Produto == produto.Codigo).OrderByDescending(d => d.Data).First();

                    PedidoItens.Remove(item);

                    if (produto.Tipo == 1)
                    {
                        var BuscaProduto = Bebidas.Where(p => p.Codigo == produto.Codigo).FirstOrDefault();
                        BuscaProduto.Quantidade = BuscaProduto.Quantidade - 1;
                    }
                    else if (produto.Tipo == 2)
                    {
                        var BuscaProduto = Cardapio.Where(p => p.Codigo == produto.Codigo).FirstOrDefault();
                        BuscaProduto.Quantidade = BuscaProduto.Quantidade - 1;
                    }

                    OnPropertyChange(nameof(produto));
                    OnPropertyChange(nameof(Cardapio));
                }
            }
            catch (Exception e)
            {
                App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private void ExecuteAumentarQtdeConsumoCommand(Produto produto)
        {
            try
            {
                if (produto != null && produto.Inativo != "S")
                {
                    ItensPedidos.Clear();
                    produto.Quantidade = produto.Quantidade + 1;
                    PedidoItem = new PedidoItem { Valor = produto.Valor, Mesa = Mesa, Ficha = Ficha, Nome = Nome, Quantidade = 1, Produto = produto.Codigo, CodGrupo = produto.CodGrupo, CodSecao = produto.CodSecao, Descricao = produto.Descricao, Vendedor = App.Usuario.Vendedor, Maquina = Convert.ToInt32(App.Config.Maquina) };
                    PedidoItens.Add(PedidoItem);
                    PedidoItem.Item = PedidoItens.Count();

                    if (produto.Tipo == 1)
                    {
                        var BuscaProduto = Bebidas.Where(p => p.Codigo == produto.Codigo).FirstOrDefault();
                        BuscaProduto.Quantidade = BuscaProduto.Quantidade + 1;
                    }
                    else if (produto.Tipo == 2)
                    {
                        var BuscaProduto = Cardapio.Where(p => p.Codigo == produto.Codigo).FirstOrDefault();
                        BuscaProduto.Quantidade = BuscaProduto.Quantidade + 1;
                    }

                    var itens = PedidoItens.Where(p => p.Produto == produto.Codigo).OrderByDescending(x => x.Data);

                    itens.ForEach(sc => ItensPedidos.Add(sc));

                    if (produto.AbrirAdicionais == "S")
                    {
                        AbrirAdicionais(ItensPedidos);
                    }

                }

                OnPropertyChange(nameof(produto));
                OnPropertyChange(nameof(Cardapio));
            }
            catch (Exception e)
            {
                App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        async void AbrirAdicionais(ObservableCollection<PedidoItem> pedidoItens)
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;
                    var page = new AdicionaisPage(pedidoItens);
                    await PopupNavigation.Instance.PushAsync(page);
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteCancelarItemCommand(Produto produto)
        {
            if (App.Parametro.PermitirCancelarItemFicha != "S")
            {
                await App.Current.MainPage.DisplayAlert("AmiGo", "Sem permissão para cancelar", "OK");
                return;
            }

            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;
                    int quantidade = Convert.ToInt32(produto.Quantidade);
                    var item = new PedidoItem { Valor = produto.Valor, Mesa = Mesa, Ficha = Ficha, Nome = Nome, Quantidade = quantidade, Produto = produto.Codigo, CodGrupo = produto.CodGrupo, CodSecao = produto.CodSecao, Descricao = produto.Descricao, Vendedor = App.Usuario.Vendedor, Maquina = Convert.ToInt32(App.Config.Maquina) };
                    var page = new CancelarItemPage(item);
                    await PopupNavigation.Instance.PushAsync(page);
                    IsBusy = false;
                    return;
                }

            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                IsBusy = false;
            }
        }


    }
}
