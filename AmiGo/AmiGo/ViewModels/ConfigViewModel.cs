﻿using AmiGo.Helpers;
using AmiGo.Models;
using AmiGo.Views;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AmiGo.ViewModels
{
    public class ConfigViewModel : BaseViewModel
    {
        public Command GravarIpCommand { get; }

        private string _ip;
        public string Ip
        {
            get { return _ip; }
            set
            {
                SetProperty(ref _ip, value);
                OnPropertyChange(nameof(Ip));
            }
        }

        private string _device;
        public string Device
        {
            get { return _device; }
            set
            {
                SetProperty(ref _device, value);
                OnPropertyChange(nameof(Device));
            }
        }


        public ConfigViewModel()
        {
            GravarIpCommand = new Command(ExecuteGravarIpCommand);
            WhiteConfig();
        }

        async void WhiteConfig()
        {
            try
            {
                //Le a Configuração
                if (App.BaseUrl != null)
                    Ip = App.BaseUrl.Replace("http://", "").Replace(":2390/api/", "");
                else
                    Ip = "192.168.0.8";

                if ((App.Config != null) && (App.Config.Maquina != null))
                    Device = App.Config.Maquina;
                else
                    Device = "1";
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }
        private async void ExecuteGravarIpCommand()
        {
            try
            {
                App.BaseUrl = "http://" + Ip + ":2390/api/";
                App.Config = new Config
                {
                    Maquina = Device,
                    Host = App.BaseUrl
                };

                string output = JsonConvert.SerializeObject(new Config { Host = App.BaseUrl, Maquina = Device });

                await PCLHelper.WriteTextAllAsync("Config.txt", output);
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }
            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }
    }
}
