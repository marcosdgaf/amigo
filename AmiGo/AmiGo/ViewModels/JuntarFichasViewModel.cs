﻿using Acr.UserDialogs;
using AmiGo.Models;
using AmiGo.Services;
using Microsoft.AppCenter.Crashes;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;


namespace AmiGo.ViewModels
{
    public class JuntarFichasViewModel : BaseViewModel
    {
        public bool IsBusy { get; set; }
        public Command CancelaCommand { get; }
        public Command JuntaFichasCommand { get; }

        private string _pesquisa;
        public string Pesquisa
        {
            get { return _pesquisa; }
            set
            {
                SetProperty(ref _pesquisa, value);
                OnPropertyChange(nameof(Pesquisa));
            }
        }
        private ObservableCollection<Ficha> _fichas;
        public ObservableCollection<Ficha> Fichas
        {
            get { return _fichas; }
            set
            {
                SetProperty(ref _fichas, value);
                OnPropertyChange(nameof(Fichas));
            }
        }
        public JuntaFicha JuntaFicha { get; set; }
        private string Mesa { get; set; }

        public JuntarFichasViewModel(string mesa)
        {
            CancelaCommand = new Command(ExecuteCancelaCommand);
            JuntaFichasCommand = new Command(ExecuteJuntaFichasCommand);
            Fichas = new ObservableCollection<Ficha>();
            JuntaFicha = new JuntaFicha();
            CarregaFichas(mesa);
            Mesa = mesa;
        }

        private async void ExecuteJuntaFichasCommand()
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;

                    PromptResult pResult = await UserDialogs.Instance.PromptAsync(new PromptConfig
                    {
                        InputType = InputType.Number,
                        Title = "Informe o número da nova Ficha",
                        Text = "",
                        OkText = "Confirmar",
                        CancelText = "Cancelar"

                    });

                    if (pResult.Ok && !string.IsNullOrWhiteSpace(pResult.Text))
                    {

                        JuntaFicha.NovaFicha = Convert.ToInt32(pResult.Text);
                        JuntaFicha.Usuario = App.Usuario.Id;
                        JuntaFicha.Vendedor = App.Usuario.Vendedor;

                        var fichasSelecionadas = Fichas.Where(p => p.Selecionado == true).ToList();
                        foreach (var ficha in fichasSelecionadas)
                        {
                            JuntaFicha.Fichas.Add(ficha);
                        }

                        using (var objDialog = UserDialogs.Instance.Loading("Juntando Fichas", null, null, true, MaskType.Black))
                        {
                            var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                            var result = await amiGoApiService.PostJuntarFichasAsync(JuntaFicha);

                            if (result.Sucesso)
                            {
                                await App.Current.MainPage.DisplayAlert("AmiGo", "Nova ficha gerada com sucesso", "OK");
                                if (Mesa != "0")
                                {
                                    MessagingCenter.Send<App>((App)App.Current, "Atualizar");
                                }
                            }
                            else
                            {
                                await App.Current.MainPage.DisplayAlert("AmiGo", "Não foi possível juntar as fichas =/ @\n" + result.Mensagem, "OK");
                            }
                            await App.Current.MainPage.Navigation.PopPopupAsync();
                        }
                    }
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteCancelaCommand()
        {
            await App.Current.MainPage.Navigation.PopPopupAsync();
        }
        private async void CarregaFichas(string mesa)
        {
            try
            {
                using (var objDialog = UserDialogs.Instance.Loading("Carregando Fichas", null, null, true, MaskType.Black))
                {
                    Fichas.Clear();
                    var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                    var result = await amiGoApiService.GetFichasMesaAsync(mesa);

                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            Fichas.Add(item);
                        }
                    }

                    OnPropertyChange(nameof(Fichas));
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro ao Verificar as Fichas", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }
    }
}
