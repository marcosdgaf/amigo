﻿using Acr.UserDialogs;
using AmiGo.Models;
using AmiGo.Services;
using AmiGo.Views;
using Microsoft.AppCenter.Crashes;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmiGo.ViewModels
{
    public class FichaViewModel : BaseViewModel
    {
        public Command CancelaCommand { get; }
        public Command PedidoCommand { get; }
        public Command GravaFichaCommand { get; }
        public Command EditaNomeCommand { get; }
        public Command JuntarFichaCommand { get; }

        public string Mesa { get; set; }
        public bool IsBusy { get; set; }

        private ObservableCollection<Ficha> _fichas;
        public ObservableCollection<Ficha> Fichas
        {
            get { return _fichas; }
            set
            {
                SetProperty(ref _fichas, value);
                OnPropertyChange(nameof(Fichas));
            }
        }

        private string _nome;
        public string Nome
        {
            get { return _nome; }
            set
            {
                SetProperty(ref _nome, value);
            }
        }

        private string _ficha;
        public string Ficha
        {
            get { return _ficha; }
            set
            {
                SetProperty(ref _ficha, value);
            }
        }
        public bool IsJuntarFichaVisible { get; set; }

        public FichaViewModel(string mesa)
        {
            Mesa = mesa;
            CancelaCommand = new Command(ExecuteCancelaCommand);
            PedidoCommand = new Command<Ficha>(ExecutePedidoCommand);
            GravaFichaCommand = new Command(ExecuteGravaFichaCommand);
            EditaNomeCommand = new Command<Ficha>(ExecuteEditaNomeCommand);
            Fichas = new ObservableCollection<Ficha>();
            CarregaFichas(mesa);
            JuntarFichaCommand = new Command(ExecuteJuntarFichaCommand);
            IsJuntarFichaVisible = App.Parametro.Ficha;
        }

        private async void ExecuteEditaNomeCommand(Ficha ficha)
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;

                    PromptResult pResult = await UserDialogs.Instance.PromptAsync(new PromptConfig
                    {
                        InputType = InputType.Name,
                        Title = "Alterar Nome",
                        Text = ficha.Nome,
                        OkText = "Alterar",
                        CancelText = "Cancelar"
                    });

                    if (pResult.Ok && !string.IsNullOrWhiteSpace(pResult.Text))
                    {
                        ficha.Nome = pResult.Text;
                        var amiGoApiService = DependencyService.Get<IAmiGoApiService>();

                        var alteraNome = await amiGoApiService.PutAlteraFichaAsync(new Ficha { Numero = ficha.Numero, Nome = pResult.Text, Mesa = ficha.Mesa });

                        if (alteraNome)
                        {
                            CarregaFichas(Mesa);
                        }
                        else
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Não foi possível alterar o nome =/", "OK");
                        }
                    }
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteGravaFichaCommand()
        {
            try
            {
                if (string.IsNullOrEmpty(Ficha) == true)
                {
                    await App.Current.MainPage.DisplayAlert("AmiGo", "Obrigatório informar o número da Ficha", "OK");
                    return;
                }
                var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                var result = await amiGoApiService.GetFichaAsync(Ficha);

                if (result == null)
                {
                    if (App.Parametro.BloquearAberturaFicha == "S")
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha não está aberta. Por favor abrir a ficha pelo terminal", "OK");
                        return;
                    }
                    if (Convert.ToInt32(Ficha) == 0)
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Obrigatório informar o número da Ficha", "OK");
                        return;
                    }

                    var ficha = new Ficha { Mesa = Convert.ToInt32(Mesa), Numero = Convert.ToInt32(Ficha), Nome = Nome };
                    ExecutePedidoCommand(ficha);
                    Fichas.Add(ficha);
                    Ficha = "";
                    Nome = "";
                }
                else
                {
                    if (result.Bloqueada)
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha Bloqueada até as " + Convert.ToString(result.DataFinalizacao), "OK");
                        return;
                    }
                    var answer = await UserDialogs.Instance.ConfirmAsync("Ficha já aberta em outra mesa, mover para a atual?", null, "Sim", "Não");
                    if (answer)
                    {

                        var trocaMesa = await amiGoApiService.PutAlteraFichaAsync(new Ficha { Mesa = Convert.ToInt32(Mesa), Numero = Convert.ToInt32(Ficha) });
                        result.Mesa = Convert.ToInt32(Mesa);
                        if (trocaMesa)
                        {
                            CarregaFichas(Mesa);
                            ExecutePedidoCommand(result);
                        }
                        else
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Não foi possível realizar a troca de Mesa =/", "OK");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro ao gravar a Ficha", e.Message, "OK");
                Crashes.TrackError(e);
            }

        }

        private async void ExecuteCancelaCommand()
        {
            try
            {
                await App.Current.MainPage.Navigation.PopPopupAsync();
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }
        private async void ExecutePedidoCommand(Ficha ficha)
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;
                    var page = new PedidoPage(Convert.ToString(ficha.Mesa), Convert.ToString(ficha.Numero), ficha.Nome);
                    await PopupNavigation.Instance.PushAsync(page);
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }
        public async void CarregaFichas(string mesa)
        {
            try
            {
                using (var objDialog = UserDialogs.Instance.Loading("Carregando Fichas", null, null, true, MaskType.Black))
                {
                    Fichas.Clear();
                    var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                    var result = await amiGoApiService.GetFichasMesaAsync(mesa);

                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            Fichas.Add(item);
                        }
                    }

                    OnPropertyChange(nameof(Fichas));
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro ao Verificar as Fichas", e.Message, "OK");
                Crashes.TrackError(e);
            }
        } 

        private async void ExecuteJuntarFichaCommand()
        {
            var pageJuntarFicha = new JuntarFichasPage(Mesa);
            await PopupNavigation.Instance.PushAsync(pageJuntarFicha);
            return;
        }
    }
}
