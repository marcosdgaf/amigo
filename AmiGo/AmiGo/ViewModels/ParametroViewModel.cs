﻿using AmiGo.Models;
using Microsoft.AppCenter.Crashes;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace AmiGo.ViewModels
{
    public class ParametroViewModel : BaseViewModel
    {

        public Command GravarParametroCommand { get; }
        public Command AumentarDiminuirMesasCommand { get; }

        private bool _pesquisaDescricao;
        public bool PesquisaDescricao
        {
            get { return _pesquisaDescricao; }
            set
            {
                SetProperty(ref _pesquisaDescricao, value);
                OnPropertyChange(nameof(PesquisaDescricao));
            }
        }

        private bool _ordenarDescricao;
        public bool OrdenarDescricao
        {
            get { return _ordenarDescricao; }
            set
            {
                SetProperty(ref _ordenarDescricao, value);
                OnPropertyChange(nameof(OrdenarDescricao));
            }
        }

        private int _tabMesas;
        public int TabMesas
        {
            get { return _tabMesas; }
            set
            {
                SetProperty(ref _tabMesas, value);
                OnPropertyChange(nameof(TabMesas));
            }
        }

        private bool _pesquisarPorFicha;
        public bool PesquisarPorFicha
        {
            get { return _pesquisarPorFicha; }
            set
            {
                SetProperty(ref _pesquisarPorFicha, value);
                OnPropertyChange(nameof(PesquisarPorFicha));
            }
        }

        public bool IsPesquisarPorFichaVisible;

        private bool _ordenarAdicionalDescricao;
        public bool OrdenarAdicionalDescricao
        {
            get { return _ordenarAdicionalDescricao; }
            set
            {
                SetProperty(ref _ordenarAdicionalDescricao, value);
                OnPropertyChange(nameof(OrdenarAdicionalDescricao));
            }
        }

        private bool _telaInicialPadao;
        public bool TelaInicialPadao
        {
            get { return _telaInicialPadao; }
            set
            {
                SetProperty(ref _telaInicialPadao, value);
                OnPropertyChange(nameof(TelaInicialPadao));
            }
        }

        private bool _telaInicialFichasVisible;
        public bool TelaInicialFichasVisible
        {
            get { return _telaInicialFichasVisible; }
            set
            {
                SetProperty(ref _telaInicialFichasVisible, value);
                OnPropertyChange(nameof(TelaInicialFichasVisible));
            }
        }




        public ParametroViewModel()
        {
            GravarParametroCommand = new Command(ExecuteGravarParametroCommand);
            AumentarDiminuirMesasCommand = new Command<string>(ExecuteAumentarDiminuirMesasCommand);
            ConfiguraParametro();
        }

        private void ExecuteAumentarDiminuirMesasCommand(string Tipo)
        {
            try
            {
                if (Tipo == "0")
                {
                    if (TabMesas != 1)
                        TabMesas--;
                }
                else
                {
                    if (TabMesas < 17)
                        TabMesas++;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private void ConfiguraParametro()
        {
            try
            {
                TabMesas = App.Parametro.TabMesas;
                PesquisaDescricao = App.Parametro.PesquisaDescricao;
                OrdenarDescricao = App.Parametro.OrdenarDescricao;
                PesquisarPorFicha = App.Parametro.PesquisarPorFicha;
                IsPesquisarPorFichaVisible = App.Parametro.Ficha;
                OrdenarAdicionalDescricao = App.Parametro.OrdenarAdicionalDescricao;
                TelaInicialFichasVisible = App.Parametro.TelaInicialFichasVisible;
                TelaInicialPadao = App.Parametro.TelaInicialPadao == 0 ? true : false;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteGravarParametroCommand()
        {
            try
            {
                App.Parametro.PesquisaDescricao = PesquisaDescricao;
                App.Parametro.TabMesas = TabMesas;
                App.Parametro.OrdenarDescricao = OrdenarDescricao;
                App.Parametro.PesquisarPorFicha = PesquisarPorFicha;
                App.Parametro.OrdenarAdicionalDescricao = OrdenarAdicionalDescricao;
                App.Parametro.TelaInicialFichasVisible = TelaInicialFichasVisible;
                App.Parametro.TelaInicialPadao = TelaInicialFichasVisible ? TelaInicialPadao ? 0 : 1 : 0;

                await App.Database.SaveParametroAsync(App.Parametro);
                await App.Current.MainPage.Navigation.PopPopupAsync();
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }
    }
}
