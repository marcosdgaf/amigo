﻿using AmiGo.Models;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace AmiGo.ViewModels
{
    public class PizzaSaboresViewModel : BaseViewModel
    {
        public Command CancelaCommand { get; }
        public Command ConfirmarCommand { get; }
        public PedidoItem PedidoItem { get; set; }

        private ObservableCollection<Produto> _listaProdutos;
        private Produto verificaProduto;

        public ObservableCollection<Produto> ListaProdutos
        {
            get { return _listaProdutos; }
            set
            {
                SetProperty(ref _listaProdutos, value);
                OnPropertyChange(nameof(ListaProdutos));
            }
        }
        
        public PizzaSaboresViewModel(PedidoItem item)
        {
            PedidoItem = item;
            CancelaCommand = new Command(ExecuteCancelaCommand);
            ConfirmarCommand = new Command(ExecuteConfirmarCommand);
            ListaProdutos = new ObservableCollection<Produto>();
           //CarregaProdutos(PedidoItem);

        }

        //private async void CarregaProdutos(PedidoItem pedidoItem)
        //{
        //    //ListaProdutosSelecionados.Clear();            

        //    ListaProdutos.Clear();
        //    var produtos = await App.Database.GetProdutosGrupo(pedidoItem.CodSecao, pedidoItem.CodGrupo);
        //    if (produtos != null)
        //    {
        //        foreach (var produto in produtos)
        //        {
        //            if (produto.Codigo != pedidoItem.Produto)
        //            {
        //                ListaProdutos.Add(produto);
        //                OnPropertyChange(nameof(ListaProdutos));
        //            }
        //        }

        //        // PedidoItem.ListaProdutos = ListaProdutos;
        //    }
        //}

        public void AdicionaProduto(Produto produto)
        {

            var qtdItens = PedidoItem.PizzasSabores.Where(p => p.Selecionado).ToList();

            if (qtdItens.Count > (PedidoItem.QtdSaboresPizza-1))
                produto.Selecionado = false;

            

            OnPropertyChange(nameof(PedidoItem.PizzasSabores));
        }

        //public void RemoveProduto(Produto produto)
        //{
        //    PedidoItem.PizzasSabores.Remove(produto);
        //    OnPropertyChange(nameof(PedidoItem.PizzasSabores));
        //}

        private async void ExecuteConfirmarCommand()
        {
            await App.Current.MainPage.Navigation.PopPopupAsync();
        }

        private async void ExecuteCancelaCommand()
        {
           await App.Current.MainPage.Navigation.PopPopupAsync();
        }
    }
}
