﻿using Acr.UserDialogs;
using AmiGo.Models;
using AmiGo.Services;
using Microsoft.AppCenter.Crashes;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace AmiGo.ViewModels
{
    public class FinalizadoresViewModel : BaseViewModel
    {
        public string Mesa { get; set; }
        public string Ficha { get; set; }
        public string Nome { get; set; }

        public bool IsBusy { get; set; }

        private ObservableCollection<Finalizador> _finalizadores;
        public ObservableCollection<Finalizador> Finalizadores
        {
            get { return _finalizadores; }
            set
            {
                SetProperty(ref _finalizadores, value);
                OnPropertyChange(nameof(Finalizadores));
            }
        }

        private ObservableCollection<Produto> _consumacoes;
        public ObservableCollection<Produto> Consumacoes
        {
            get { return _consumacoes; }
            set
            {
                SetProperty(ref _consumacoes, value);
                OnPropertyChange(nameof(Consumacoes));
            }
        }
        private decimal _totalMesa;
        public decimal TotalMesa
        {
            get { return _totalMesa; }
            set
            {
                SetProperty(ref _totalMesa, value);
                OnPropertyChange(nameof(TotalMesa));
            }
        }
        private decimal _totalConsumo;
        public decimal TotalConsumo
        {
            get { return _totalConsumo; }
            set
            {
                SetProperty(ref _totalConsumo, value);
                OnPropertyChange(nameof(TotalConsumo));
            }
        }


        private decimal _totalServico;
        public decimal TotalServico
        {
            get { return _totalServico; }
            set
            {
                SetProperty(ref _totalServico, value);
                OnPropertyChange(nameof(TotalServico));
            }
        }

        private int _pessoas;
        public int Pessoas
        {
            get { return _pessoas; }
            set
            {
                SetProperty(ref _pessoas, value);
                OnPropertyChange(nameof(Pessoas));
            }
        }

        private decimal _servico;
        public decimal Servico
        {
            get { return _servico; }
            set
            {
                if (SetProperty(ref _servico, value))
                    RecalculaServico();
                OnPropertyChange(nameof(Servico));
            }
        }

        public Command AumentarDiminuirPessoasCommand { get; }
        public Command AumentarDiminuirServicoCommand { get; }
        public Command FinalizadorCommand { get; }
        public Command CancelaFinalizadorCommand { get; }
        public Command CancelaCommand { get; }
        public Command GeraPreVendaCommand { get; }

        public FinalizadoresViewModel(string mesa, string ficha, string nome)
        {
            Consumacoes = new ObservableCollection<Produto>();
            Finalizadores = new ObservableCollection<Finalizador>();
            TotalConsumo = 0;
            TotalServico = 0;
            TotalMesa = 0;
            Pessoas = 1;
            Servico = App.Parametro.MinGarcom;

            Mesa = mesa;
            Ficha = ficha;
            Nome = nome;
            VerificaConsumacao(Mesa, Ficha);
            CarregaFinalizadores(App.Config.Maquina);

            AumentarDiminuirPessoasCommand = new Command<string>(ExecuteAumentarDiminuirPessoasCommand);
            AumentarDiminuirServicoCommand = new Command<string>(ExecuteAumentarDiminuirServicoCommand);
            FinalizadorCommand = new Command<Finalizador>(ExecuteFinalizadorCommand);
            CancelaFinalizadorCommand = new Command<Finalizador>(ExecuteCancelaFinalizadorCommand);
            CancelaCommand = new Command(ExecuteCancelaCommand);
            GeraPreVendaCommand = new Command(ExecuteGeraPreVendaCommand);
        }
        private async void ExecuteGeraPreVendaCommand()
        {
            try
            {
                var TotalPago = Finalizadores.Sum(s => s.Valor);
                if ((TotalPago - TotalMesa) < 0)
                {
                    await App.Current.MainPage.DisplayAlert("Pré-venda","Valor dos finalizadores menor do que o da venda ", "OK");
                    return;
                }

                var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                //var result = await amiGoApiService.PutFechamentoAsync(Mesa, Ficha, Servico, Pessoas);

                var preVenda = new PreVenda
                {
                    Ficha = Convert.ToInt32(Ficha),
                    Mesa = Convert.ToInt32(Mesa),
                    PorcentagemServico = Convert.ToDecimal(Servico),
                    QtdePessoas = Pessoas,
                    Usuario = App.Usuario.Id,
                    Vendedor = App.Usuario.Vendedor,
                    Maquina = Convert.ToInt32(App.Config.Maquina)
                };

                foreach (var item in Finalizadores.Where(p => p.Valor > 0))
                {
                    preVenda.Finalizadores.Add(item);
                }

                var answer = await UserDialogs.Instance.ConfirmAsync("Emitir Cupom fiscal?", null, "Sim", "Não");
                if (answer)
                {
                    string InformaCPF = await Application.Current.MainPage.DisplayPromptAsync(
                     "CPF:",
                     "",
                     "Confirmar",
                     "Não informar",
                     initialValue:"" ,
                     maxLength: 18,
                     keyboard: Keyboard.Numeric);
                    if (InformaCPF != null)
                    {                        
                        preVenda.CpfCnpj = InformaCPF;
                    }                    
                }
                preVenda.EmitirFiscal = answer;

                Result result = new Result();
                using (var objDialog = UserDialogs.Instance.Loading("Gerando Pré-venda", null, null, true, MaskType.Black))
                {
                    result = await amiGoApiService.PutFechamentoAsync(preVenda);
                }

                if (result.Sucesso)
                {
                    await App.Current.MainPage.DisplayAlert("", result.Mensagem, "OK");                    
                    await App.Current.MainPage.Navigation.PopAllPopupAsync();
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("Erro ao gerar a Pré-venda: ", result.Mensagem, "OK");
                }

            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteCancelaCommand()
        {
            await App.Current.MainPage.Navigation.PopPopupAsync();
        }

        private void ExecuteCancelaFinalizadorCommand(Finalizador finalizador)
        {
            finalizador.Valor = 0;
            finalizador.Troco = 0;
            OnPropertyChange(nameof(Finalizadores));
        }

        private async void ExecuteFinalizadorCommand(Finalizador finalizador)
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;

                    string result = await Application.Current.MainPage.DisplayPromptAsync(
                        finalizador.Descricao,
                        "",
                        "Confirmar",
                        "Cancelar",
                        initialValue: Convert.ToString(decimal.Round(Convert.ToDecimal(TotalMesa - Finalizadores.Sum(c => c.Valor)),2)),
                        maxLength: 10,
                        keyboard: Keyboard.Numeric);


                    if (result != null)
                    {
                        finalizador.Valor = Convert.ToDecimal(result.Replace("R$ ", ""));

                        var TotalPago = Finalizadores.Sum(s => s.Valor);
                        if ((TotalPago - TotalMesa) > 0)
                            finalizador.Troco = (TotalPago - TotalMesa);
                        else
                            finalizador.Troco =0;
                    }

                    //PromptResult pResult = await UserDialogs.Instance.PromptAsync(new PromptConfig
                    //{
                    //    InputType = InputType.Name,
                    //    Title = finalizador.Descricao,
                    //    Text = (TotalMesa - Finalizadores.Sum(c => c.Valor)).ToString(),
                    //    OkText = "Confirmar",
                    //    CancelText = "Cancelar"
                    //});


                    //if (pResult.Ok && !string.IsNullOrWhiteSpace(pResult.Text))
                    //{
                    //    finalizador.Valor = Convert.ToDecimal(pResult.Text);

                    //}
                    IsBusy = false;
                }
                OnPropertyChange(nameof(Finalizadores));
            }
            catch (Exception e)
            {
                IsBusy = false;
                Crashes.TrackError(e);
            }
        }

        private async void CarregaFinalizadores(string maquina)
        {
            var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
            var result = await amiGoApiService.GetFinalizadoresAsync(Convert.ToInt32(maquina));

            Finalizadores.Clear();
            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    item.Valor = 0;
                    item.Troco = 0;
                    Finalizadores.Add(item);
                }
            }
            OnPropertyChange(nameof(Finalizadores));
        }

        private void ExecuteAumentarDiminuirPessoasCommand(string Tipo)
        {
            try
            {
                if (Tipo == "0")
                {
                    if (Pessoas != 1)
                        Pessoas--;
                }
                else
                {
                    Pessoas++;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private void ExecuteAumentarDiminuirServicoCommand(string Tipo)
        {
            try
            {
                if (Tipo == "0")
                {
                    if (Servico != App.Parametro.MinGarcom && Servico != 0)
                        Servico--;
                    else if (Servico == App.Parametro.MinGarcom)
                        Servico = 0;

                }
                else
                {
                    if (Servico == 0)
                        Servico = App.Parametro.MinGarcom;
                    else if (Servico < App.Parametro.MaxGarcom)
                        Servico++;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }
        public async void VerificaConsumacao(string mesa, string ficha)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(mesa) == false)
                {
                    var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                    var result = await amiGoApiService.GetConsumacaoAsync(mesa, ficha);

                    Consumacoes.Clear();
                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            var grupo = await App.Database.GetGrupoAsync();
                            var produto = await App.Database.GetProdutoAsync(item.Produto);
                            if (produto != null)
                            {
                                var buscaGrupo = grupo.Where(g => g.Codigo == produto.CodGrupo && g.Secao == produto.CodSecao).FirstOrDefault();
                                if (buscaGrupo != null)
                                {
                                    produto.Grupo = grupo.Where(g => g.Codigo == produto.CodGrupo && g.Secao == produto.CodSecao).FirstOrDefault();
                                    produto.Quantidade = item.Quantidade;
                                    produto.Consumido = item.Quantidade;
                                    produto.Hora = item.Hora;
                                    Consumacoes.Add(produto);
                                }
                            }
                        }

                        TotalConsumo = result.Sum(c => c.Valor);
                        RecalculaServico();
                    }

                    //if (await CrossConnectivity.Current.IsRemoteReachable(App.Parametro.ServidorImpressao.Replace("HTTP://", "").Replace(":2386", ""), 2386))
                    //{
                    //    var imprimiu = await amiGoApiService.GetImpressaoPedidoAsync(Mesa, Ficha);
                    //}
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Não foi possivel Verificar a consumação", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        public void RecalculaServico()
        {
            if (TotalConsumo > 0)
                TotalServico = (TotalConsumo / 100) * Servico;
            TotalMesa = Decimal.Round(TotalConsumo + TotalServico,2);
        }
    }
}
