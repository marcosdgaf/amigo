﻿using Acr.UserDialogs;
using AmiGo.Models;
using AmiGo.Services;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmiGo.ViewModels
{
    public class CancelarItemViewModel : BaseViewModel
    {
        public Command CancelaCommand { get; }
        public Command AumentaQtdeCommand { get; }
        public Command DiminuiQtdeCommand { get; }
        public Command CancelarItemCommand { get; }

        public PedidoItem PedidoItem { get; set; }

        private string _justificativa;
        public string Justificativa
        {
            get { return _justificativa; }
            set
            {
                SetProperty(ref _justificativa, value);
                OnPropertyChange(nameof(Justificativa));
            }
        }
        private double QuantidadeMaxima { get; set; }
        private bool IsBusy { get; set; }


        public CancelarItemViewModel(PedidoItem pedidoItem)
        {
            CancelaCommand = new Command(ExecuteCancelaCommand);
            AumentaQtdeCommand = new Command(ExecuteAumentarQtdeCommand);
            DiminuiQtdeCommand = new Command(ExecuteDiminuirQtdeCommand);
            CancelarItemCommand = new Command(ExecuteCancelarItemCommand);

            PedidoItem = pedidoItem;
            QuantidadeMaxima = pedidoItem.Quantidade;
            PedidoItem.Quantidade = 1;
        }

        private async void ExecuteCancelaCommand()
        {
            await App.Current.MainPage.Navigation.PopPopupAsync();
        }

        private void ExecuteDiminuirQtdeCommand()
        {
            try
            {
                if (PedidoItem.Quantidade > 1)
                    PedidoItem.Quantidade += -1;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }
        private void ExecuteAumentarQtdeCommand()
        {
            try
            {
                if (PedidoItem.Quantidade < QuantidadeMaxima)
                    PedidoItem.Quantidade += +1;
            }
            catch (Exception e)
            {

                Crashes.TrackError(e);
            }
        }
        private async void ExecuteCancelarItemCommand()
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;
                    if (string.IsNullOrEmpty(Justificativa))
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Necessário informar a justificativa.", "OK");
                        IsBusy = false;
                        return;
                    }
                    PedidoItem.Observacao = Justificativa;

                    using (var objDialog = UserDialogs.Instance.Loading("Cancelando Item", null, null, true, MaskType.Black))
                    {
                        if (!await CrossConnectivity.Current.IsRemoteReachable(App.BaseUrl.ToUpper().Replace("HTTP://", "").Replace(":2390/API/", ""), 2390))
                        {
                            await Task.Delay(2000);
                            if (!await CrossConnectivity.Current.IsRemoteReachable(App.BaseUrl.ToUpper().Replace("HTTP://", "").Replace(":2390/API/", ""), 2390))
                            {
                                await App.Current.MainPage.DisplayAlert("AmiGo", "Verifique Sua Conexão", "OK");
                                IsBusy = false;
                                return;
                            }
                        }

                        var amiGoApiService = DependencyService.Get<IAmiGoApiService>();

                        var result = await amiGoApiService.PostCancelarItemAsync(PedidoItem);
                        if (result.Sucesso)
                        {
                            MessagingCenter.Send<App>((App)App.Current, "Atualizar");
                            await App.Current.MainPage.Navigation.PopPopupAsync();
                        }
                        else
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Falha ao cancelar o item: " + result.Mensagem, "OK");
                    }
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                Crashes.TrackError(e);
            }
        }

    }
}
