﻿using Acr.UserDialogs;
using AmiGo.Models;
using AmiGo.Services;
using AmiGo.Views;
using FFImageLoading.Forms;
using Microsoft.AppCenter.Crashes;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmiGo.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public Command PedidoCommand { get; }
        public Command LogoutCommand { get; }
        public Command ParametrosCommand { get; }
        public Command JuntarFichaCommand { get; }
        public Command PedidoFichaCommand { get; }
        public Command GravaFichaCommand { get; }
        public Command OpcoesCommand { get; }
        public Usuario Usuario { get; set; }
        public int NumeroMesa { get; set; }
        public bool IsBusy { get; set; }
        public bool IsBusyVerificaMesa { get; set; }
        public string ModoPesquisa { get; set; }
        public bool IsJuntarFichaVisible { get; set; }
        public bool IsTelaInicialFichasVisible { get; set; }

        private ObservableCollection<Grid> _grids;
        public ObservableCollection<Grid> Grids
        {
            get => _grids;
            set
            {
                SetProperty(ref _grids, value);
                OnPropertyChange(nameof(Grids));
            }
        }

        private string _pesquisa;
        public string Pesquisa
        {
            get => _pesquisa;
            set
            {
                SetProperty(ref _pesquisa, value);
                OnPropertyChange(nameof(Pesquisa));
            }
        }

        private ObservableCollection<CachedImage> _imgBtns;
        public ObservableCollection<CachedImage> ImgBtns
        {
            get => _imgBtns;
            set
            {
                SetProperty(ref _imgBtns, value);
                OnPropertyChange(nameof(ImgBtns));
            }
        }
        private ObservableCollection<Ficha> _fichas;
        public ObservableCollection<Ficha> Fichas
        {
            get { return _fichas; }
            set
            {
                SetProperty(ref _fichas, value);
                OnPropertyChange(nameof(Fichas));
            }
        }

        private string _pesquisaFicha;
        public string PesquisaFicha
        {
            get { return _pesquisaFicha; }
            set
            {
                SetProperty(ref _pesquisaFicha, value);
                OnPropertyChange(nameof(PesquisaFicha));
            }
        }
        private string _nome;
        public string Nome
        {
            get { return _nome; }
            set
            {
                SetProperty(ref _nome, value);
            }
        }

        private string _ficha;
        public string Ficha
        {
            get { return _ficha; }
            set
            {
                SetProperty(ref _ficha, value);
            }
        }

        private string _mesa;
        public string Mesa
        {
            get { return _mesa; }
            set
            {
                SetProperty(ref _mesa, value);
            }
        }

        public MainViewModel()
        {
            Usuario = App.Usuario;
            ModoPesquisa = App.Parametro.PesquisarPorFicha ? "Selecione a ficha desejada" : "Seleciona a mesa desejada";
            IsJuntarFichaVisible = App.Parametro.Ficha;
            IsTelaInicialFichasVisible = App.Parametro.TelaInicialFichasVisible;
            Grids = new ObservableCollection<Grid>();
            ImgBtns = new ObservableCollection<CachedImage>();
            Fichas = new ObservableCollection<Ficha>();
            LogoutCommand = new Command(ExecuteLogoutCommand);
            if (!string.IsNullOrEmpty(App.BaseUrl))
            {
                PedidoCommand = new Command<string>(ExecutePedidoCommand);
                ParametrosCommand = new Command(ExecuteParametrosCommand);
                JuntarFichaCommand = new Command(ExecuteJuntarFichaCommand);
                PedidoFichaCommand = new Command<Ficha>(ExecutePedidoFichaCommand);
                GravaFichaCommand = new Command(ExecuteGravaFichaCommand);
                OpcoesCommand = new Command<Ficha>(ExecuteOpcoesCommand);
                CarregaFichas();
            }
            CarregeMesas();
        }

        private async void ExecuteParametrosCommand()
        {
            try
            {
                var page = new ParametroPage();
                await PopupNavigation.Instance.PushAsync(page);
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private void ExecuteLogoutCommand()
        {
            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }

        private async void TimerMesas()
        {
            try
            {
                Device.StartTimer(TimeSpan.FromSeconds(10), () =>
                {
                    VerificaMesas();
                    return true;
                });
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }

        }

        private void AlteraMesa(string mesa, string status)
        {
            var Mesa = ImgBtns.Where(x => x.ClassId == mesa).FirstOrDefault();

            switch (status)
            {
                default: Mesa.Source = "MesaLivre.png"; break;
                case "P": Mesa.Source = "MesaOcupada.png"; break;
                case "E": Mesa.Source = "MesaFechamento.png"; break;
            }
        }

        private async void VerificaMesas()
        {
            try
            {
                if (!IsBusyVerificaMesa)
                {
                    IsBusyVerificaMesa = true;

                    var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                    var mesas = await amiGoApiService.GetMesasAsync();

                    if (mesas != null)
                    {
                        foreach (var mesa in mesas)
                        {
                            if (mesa.Numero > 0 && mesa.Numero <= NumeroMesa)
                            {
                                AlteraMesa(Convert.ToString(mesa.Numero), mesa.Status);
                            }
                        }

                        for (int i = 1; i <= NumeroMesa; i++)
                        {
                            var buscaMesa = mesas.Where(s => s.Numero == i).FirstOrDefault();
                            if (buscaMesa == null)
                            {
                                AlteraMesa(Convert.ToString(i), "");
                            }
                        }
                    }
                    IsBusyVerificaMesa = false;
                }
            }
            catch (Exception e)
            {
                IsBusyVerificaMesa = false;
                Crashes.TrackError(e);
            }
        }

        private async void ExecutePedidoCommand(string mesa)
        {
            try
            {
                if (!IsBusy)
                {
                    if ((App.Parametro.PesquisarPorFicha) && (!string.IsNullOrEmpty(Pesquisa)))
                    {
                        IsBusy = true;
                        Pesquisa = "";
                        var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                        var result = await amiGoApiService.GetFichaAsync(mesa);

                        if (result == null)
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha não está aberta", "OK");
                            IsBusy = false;
                            return;
                        }
                        else
                        {
                            if (result.Bloqueada)
                            {
                                await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha Bloqueada até as " + Convert.ToString(result.DataFinalizacao), "OK");
                                return;
                            }
                            var page = new PedidoPage(result.Mesa.ToString(), result.Numero.ToString(), result.Nome);
                            await PopupNavigation.Instance.PushAsync(page);

                            IsBusy = false;
                        }
                    }
                    else if (Convert.ToInt32(mesa) > 0 && Convert.ToInt32(mesa) <= NumeroMesa)
                    {
                        IsBusy = true;
                        Pesquisa = "";

                        if (App.Parametro.Ficha)
                        {
                            var page = new FichaPage(mesa);
                            await PopupNavigation.Instance.PushAsync(page);
                        }
                        else
                        {
                            var Mesa = ImgBtns.Where(x => x.ClassId == mesa).FirstOrDefault();
                            if ((Mesa.Source as FileImageSource).File == "MesaFechamento.png")
                            {
                                var toasFechamento = new ToastConfig("Mesa em fechamento!");
                                toasFechamento.SetDuration(3000);
                                toasFechamento.SetBackgroundColor(System.Drawing.Color.FromArgb(12, 131, 193));
                                UserDialogs.Instance.Toast(toasFechamento);
                                IsBusy = false;
                                return;
                            }
                            var page = new PedidoPage(mesa, "", "");
                            await PopupNavigation.Instance.PushAsync(page);
                        }
                        IsBusy = false;
                    }
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                Crashes.TrackError(e);
            }

        }
        private async void ExecutePedidoFichaCommand(Ficha ficha)
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;

                    if (ficha == null)
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha não está aberta", "OK");
                        IsBusy = false;
                        return;
                    }
                    else
                    {
                        if (ficha.Bloqueada)
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha Bloqueada até as " + Convert.ToString(ficha.DataFinalizacao), "OK");
                            return;
                        }
                        var page = new PedidoPage(Convert.ToString(ficha.Mesa), Convert.ToString(ficha.Numero), ficha.Nome);
                        await PopupNavigation.Instance.PushAsync(page);

                        IsBusy = false;
                    }
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                Crashes.TrackError(e);
            }

        }


        //Criação das mesas
        void CarregeMesas()
        {
            try
            {
                NumeroMesa = 0;
                Grids.Clear();
                ImgBtns.Clear();
                for (int i = 0; i < App.Parametro.TabMesas; i++)
                {

                    var grid = new Grid();

                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                    grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
                    grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

                    for (int x = 0; x < 6; x++)
                    {
                        for (int y = 0; y < 4; y++)
                        {
                            NumeroMesa++;

                            TapGestureRecognizer command = new TapGestureRecognizer
                            {
                                Command = PedidoCommand,
                                CommandParameter = Convert.ToString(NumeroMesa),
                                NumberOfTapsRequired = 1
                            };

                            var btnMesa = new CachedImage { Source = "MesaLivre.png", BackgroundColor = Color.Transparent, Style = (Style)Application.Current.Resources["ImgBtn"], ClassId = Convert.ToString(NumeroMesa) };
                            btnMesa.GestureRecognizers.Add(command);
                            ImgBtns.Add(btnMesa);


                            grid.Children.Add(btnMesa, y, x);

                            var lblmesa = new Label { Text = Convert.ToString(NumeroMesa), Style = (Style)Application.Current.Resources["lblMesa"] };
                            grid.Children.Add(lblmesa, y, x);
                        }
                    }
                    Grids.Add(grid);

                    OnPropertyChange(nameof(Grids));
                    OnPropertyChange(nameof(ImgBtns));
                }

                VerificaMesas();
                TimerMesas();
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                IsBusy = false;
            }
        }
        public async void CarregaFichas()
        {
            try
            {
                using (var objDialog = UserDialogs.Instance.Loading("Carregando Fichas", null, null, true, MaskType.Black))
                {
                    Fichas.Clear();
                    var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                    var result = await amiGoApiService.GetFichasMesaAsync("0");

                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in result)
                        {
                            Fichas.Add(item);
                        }
                    }

                    OnPropertyChange(nameof(Fichas));
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro ao Verificar as Fichas", e.Message, "OK");
                Crashes.TrackError(e);
                IsBusy = false;
            }
        }

        private async void ExecuteJuntarFichaCommand()
        {
            var pageJuntarFicha = new JuntarFichasPage("0");
            await PopupNavigation.Instance.PushAsync(pageJuntarFicha);
            return;
        }

        private async void ExecuteOpcoesCommand(Ficha ficha)
        {
            try
            {
                var pResult = await Application.Current.MainPage.DisplayActionSheet("AmiGô", "Cancelar", null, "Alterar Nome Ficha", "Trocar Mesa");
                if (pResult == "Alterar Nome Ficha")
                {
                    AlterarNomeFicha(ficha);
                }
                else if (pResult == "Trocar Mesa")
                {
                    AlterarMesaFicha(ficha);
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }



        private async void AlterarNomeFicha(Ficha ficha)
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;

                    PromptResult pResult = await UserDialogs.Instance.PromptAsync(new PromptConfig
                    {
                        InputType = InputType.Name,
                        Title = "Alterar Nome",
                        Text = ficha.Nome,
                        OkText = "Alterar",
                        CancelText = "Cancelar"
                    });

                    if (pResult.Ok && !string.IsNullOrWhiteSpace(pResult.Text))
                    {
                        ficha.Nome = pResult.Text;
                        var amiGoApiService = DependencyService.Get<IAmiGoApiService>();

                        var alteraNome = await amiGoApiService.PutAlteraFichaAsync(new Ficha { Numero = ficha.Numero, Nome = pResult.Text, Mesa = ficha.Mesa });

                        if (alteraNome)
                        {
                            CarregaFichas();
                        }
                        else
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Não foi possível alterar o nome =/", "OK");
                        }
                    }
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                IsBusy = false;
            }
        }

        private async void AlterarMesaFicha(Ficha ficha)
        {
            try
            {
                if (!IsBusy)
                {
                    IsBusy = true;

                    PromptResult pResult = await UserDialogs.Instance.PromptAsync(new PromptConfig
                    {
                        InputType = InputType.Number,
                        Title = "Alterar Mesa",
                        Text = ficha.Mesa.ToString(),
                        OkText = "Alterar",
                        CancelText = "Cancelar"
                    });

                    if (pResult.Ok && !string.IsNullOrWhiteSpace(pResult.Text))
                    {
                        ficha.Mesa = Convert.ToInt32(pResult.Text);
                        var amiGoApiService = DependencyService.Get<IAmiGoApiService>();

                        var trocaMesa = await amiGoApiService.PutAlteraFichaAsync(new Ficha { Numero = ficha.Numero, Nome = ficha.Nome, Mesa = Convert.ToInt32(pResult.Text) });
                        if (trocaMesa)
                        {
                            CarregaFichas();
                        }
                        else
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Não foi possível realizar a troca de Mesa =/", "OK");
                        }
                    }
                    IsBusy = false;
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                Crashes.TrackError(e);
            }
        }



        private async void ExecuteGravaFichaCommand()
        {
            try
            {
                var amiGoApiService = DependencyService.Get<IAmiGoApiService>();
                if (string.IsNullOrEmpty(Ficha) == true)
                {
                    bool verificaNumeroEmUso = false;

                    while (!verificaNumeroEmUso)
                    {
                        if (string.IsNullOrEmpty(Ficha) == true)
                        {
                            var numero = GeraNumeroAleatorio();
                            var verificaFicha = await amiGoApiService.GetFichaAsync(numero.ToString());
                            if (verificaFicha == null)
                            {
                                Ficha = numero.ToString();
                                verificaNumeroEmUso = true;
                            }
                        }
                        else
                        {
                            verificaNumeroEmUso = true;
                        }
                    }
                }

                var result = await amiGoApiService.GetFichaAsync(Ficha);

                if (result == null)
                {
                    if (App.Parametro.BloquearAberturaFicha == "S")
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha não está aberta. Por favor abrir a ficha pelo terminal", "OK");
                        return;
                    }
                    if (Convert.ToInt32(Ficha) == 0)
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Obrigatório informar o número da Ficha", "OK");
                        return;
                    }

                    if (string.IsNullOrEmpty(Mesa) == true)
                    {
                        Mesa = "0";
                    }

                    var ficha = new Ficha { Mesa = Convert.ToInt32(Mesa), Numero = Convert.ToInt32(Ficha), Nome = Nome };
                    Fichas.Add(ficha);
                    ExecutePedidoFichaCommand(ficha);
                    Ficha = "";
                    Nome = "";
                    Mesa = "";

                    MessagingCenter.Send<App>((App)App.Current, "AtualizarTela");
                }
                else
                {
                    if (result.Bloqueada)
                    {
                        await App.Current.MainPage.DisplayAlert("AmiGo", "Ficha Bloqueada até as " + Convert.ToString(result.DataFinalizacao), "OK");
                        return;
                    }
                    var answer = await UserDialogs.Instance.ConfirmAsync("Ficha já aberta em outra mesa, mover para a atual?", null, "Sim", "Não");
                    if (answer)
                    {
                        if (string.IsNullOrEmpty(Mesa) == true)
                        {
                            Mesa = "0";
                        }
                        var trocaMesa = await amiGoApiService.PutAlteraFichaAsync(new Ficha { Mesa = Convert.ToInt32(Mesa), Numero = Convert.ToInt32(Ficha) });
                        result.Mesa = Convert.ToInt32(Mesa);
                        if (trocaMesa)
                        {
                            CarregaFichas();
                            ExecutePedidoFichaCommand(result);
                        }
                        else
                        {
                            await App.Current.MainPage.DisplayAlert("AmiGo", "Não foi possível realizar a troca de Mesa =/", "OK");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro ao gravar a Ficha", e.Message, "OK");
                Crashes.TrackError(e);
                IsBusy = false;
            }
        }

        private int GeraNumeroAleatorio()
        {
            var rnd = new Random();
            return rnd.Next(1000);
        }
    }
}
