﻿using AmiGo.CustomRenderes;
using AmiGo.Models;
using AmiGo.Views;
using FFImageLoading.Forms;
using Microsoft.AppCenter.Crashes;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace AmiGo.ViewModels
{
    public class AdicionaisViewModel : BaseViewModel
    {
        public Command CancelaCommand { get; }
        public Command AplicarTodosObsCommand { get; }

        public Command AumentarQtdeCommand { get; }
        public Command DiminuirQtdeCommand { get; }

        public Command AumentarQtdeObsCommand { get; }
        public Command DiminuirQtdeObsCommand { get; }


        public int IdObservacao { get; set; }

        private ObservableCollection<PedidoItem> _pedidoItens;
        public ObservableCollection<PedidoItem> PedidoItens
        {
            get { return _pedidoItens; }
            set
            {
                SetProperty(ref _pedidoItens, value);
                OnPropertyChange(nameof(PedidoItens));
            }
        }

        private PedidoItem _pedidoItem;
        public PedidoItem PedidoItem
        {
            get { return _pedidoItem; }
            set
            {
                SetProperty(ref _pedidoItem, value);
                OnPropertyChange(nameof(PedidoItem));
            }
        }


        private Adicional _adicional;
        public Adicional Adicional
        {
            get { return _adicional; }
            set
            {
                SetProperty(ref _adicional, value);
                OnPropertyChange(nameof(Adicional));
            }
        }
        private ObservableCollection<StackLayout> _stks;
        public ObservableCollection<StackLayout> Stks
        {
            get { return _stks; }
            set
            {
                SetProperty(ref _stks, value);
                OnPropertyChange(nameof(Stks));
            }
        }

        public AdicionaisViewModel(ObservableCollection<PedidoItem> pedidoItens)
        {
            Stks = new ObservableCollection<StackLayout>();
            if (pedidoItens != null)
            {
                PedidoItens = pedidoItens;
                CarregaItens();
            }
            CancelaCommand = new Command(ExecuteCancelaCommand);
            Adicional = new Adicional();
            AumentarQtdeCommand = new Command<Adicional>(ExecuteAumentarQtdeCommand);
            DiminuirQtdeCommand = new Command<Adicional>(ExecuteDiminuirQtdeCommand);

            AumentarQtdeObsCommand = new Command<ProdutoObservacao>(ExecuteAumentarQtdeObsCommand);
            DiminuirQtdeObsCommand = new Command<ProdutoObservacao>(ExecuteDiminuirQtdeObsCommand);
        }

        private async void ExecuteDiminuirQtdeObsCommand(ProdutoObservacao observacao)
        {
            try
            {
                if ((observacao != null && observacao.Quantidade > 0))
                {
                    observacao.Quantidade = observacao.Quantidade - 1;
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteAumentarQtdeObsCommand(ProdutoObservacao observacao)
        {
            try
            {
                if (observacao != null)
                    observacao.Quantidade = observacao.Quantidade + 1;

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteAplicarTodosObsCommand(string item)
        {
            try
            {

                foreach (var stk in Stks)
                {
                    foreach (var i in stk.Children)
                    {

                        if (i.ClassId != null && i.ClassId != "")
                        {
                            if (i.ClassId == "StackOBS")
                            {
                                StackLayout stackObs = i as StackLayout;
                                foreach (var child in stackObs.Children)
                                {
                                    if (child.ClassId != null && child.ClassId != "")
                                    {
                                        Editor obs = child as Editor;
                                        var pedidoitem = PedidoItens.Where(p => Convert.ToString(p.Item) == child.ClassId).FirstOrDefault();
                                        if (pedidoitem != null)
                                            pedidoitem.Observacao = obs.Text;
                                    }
                                }
                            }
                        }
                    }
                }

                var ItemObs = PedidoItens.Where(p => Convert.ToString(p.Item) == item).FirstOrDefault();

                var ItensAtualizar = PedidoItens.Where(p => Convert.ToString(p.Item) != item);

                foreach (var pedidoItem in ItensAtualizar)
                {
                    pedidoItem.ProdutoObservacao = ItemObs.ProdutoObservacao;
                    pedidoItem.Observacao = ItemObs.Observacao;

                }
                CarregaItens();
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        public async void CarregaItens()
        {
            try
            {
                Stks.Clear();
                foreach (var item in PedidoItens)
                {
                    var layout = new StackLayout { BackgroundColor = Color.FromHex("#fff2ea") };
                    #region Observações
                    var lvwObservacoes = new ListView
                    {
                        HasUnevenRows = false,
                        SeparatorColor = Color.Transparent,
                        BackgroundColor = Color.Transparent,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        RowHeight = 60
                    };


                    if (item.ProdutoObservacao.Count == 0)
                    {
                        var observacoes = await App.Database.GetObservacaoAsync(item.CodSecao, item.CodGrupo, item.Produto);
                        if (App.Parametro.OrdenarAdicionalDescricao)
                        {
                            observacoes.Sort((x, y) => string.Compare(x.Observacao, y.Observacao));
                        }

                        foreach (var obs in observacoes)
                        {
                            item.ProdutoObservacao.Add(obs);
                        }
                    }

                    OnPropertyChange(nameof(item.ProdutoObservacao));
                    OnPropertyChange(nameof(item));

                    lvwObservacoes.ItemsSource = item.ProdutoObservacao;

                    var observacaoTemplante = new DataTemplate(() =>
                    {
                        var grid = new Grid { Margin = new Thickness(0, 0, 0, 0), BackgroundColor = Color.Transparent, ColumnSpacing = 0, RowSpacing = 0 };
                        grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

                        var lblObs = new Label { Style = (Style)Application.Current.Resources["lblStyle"] };
                        lblObs.SetBinding(Label.TextProperty, "Observacao");
                        var gridQuantidade = new Grid { Margin = new Thickness(0, 0, 0, 0), BackgroundColor = Color.Transparent, ColumnSpacing = 0, RowSpacing = 0 };

                        gridQuantidade.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        gridQuantidade.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        gridQuantidade.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });


                        TapGestureRecognizer tapDiminuir = new TapGestureRecognizer
                        {
                            NumberOfTapsRequired = 1

                        };


                        TapGestureRecognizer tapAumentar = new TapGestureRecognizer
                        {
                            NumberOfTapsRequired = 1
                        };

                        Binding objBindingTapDiminuir = new Binding()
                        {
                            Source = lvwObservacoes,
                            Path = "BindingContext.DiminuirQtdeObsCommand"
                        };

                        tapDiminuir.SetBinding(TapGestureRecognizer.CommandProperty, objBindingTapDiminuir);
                        tapDiminuir.SetBinding(TapGestureRecognizer.CommandParameterProperty, new Binding { Path = "." });

                        Binding objBindingTapAumentar = new Binding()
                        {
                            Source = lvwObservacoes,
                            Path = "BindingContext.AumentarQtdeObsCommand"
                        };

                        tapAumentar.SetBinding(TapGestureRecognizer.CommandProperty, objBindingTapAumentar);
                        tapAumentar.SetBinding(TapGestureRecognizer.CommandParameterProperty, new Binding { Path = "." });

                        var imgMenos = new CachedImage
                        {
                            Source = "BotaoMenos.png",
                            Style = (Style)Application.Current.Resources["ImgBtnList"],
                            HorizontalOptions = LayoutOptions.Start
                        };

                        var imgMais = new CachedImage
                        {
                            Source = "Adicionar.png",
                            Style = (Style)Application.Current.Resources["ImgBtnList"],
                            HorizontalOptions = LayoutOptions.End
                        };

                        imgMenos.GestureRecognizers.Add(tapDiminuir);
                        imgMais.GestureRecognizers.Add(tapAumentar);

                        var lblQuantidade = new Label
                        {
                            Style = (Style)Application.Current.Resources["lblStyle"],
                            TextColor = Color.FromHex("#f37021"),
                            HorizontalTextAlignment = TextAlignment.Center,
                            Margin = new Thickness(8, 0, 8, 0),
                        };

                        lblQuantidade.SetBinding(Label.TextProperty, "Quantidade");

                        gridQuantidade.Children.Add(imgMenos, 0, 0);
                        gridQuantidade.Children.Add(lblQuantidade, 1, 0);
                        gridQuantidade.Children.Add(imgMais, 2, 0);


                        grid.Children.Add(lblObs, 0, 0);
                        grid.Children.Add(gridQuantidade, 1, 0);

                        return new ViewCell { View = grid };
                    });
                    lvwObservacoes.ItemTemplate = observacaoTemplante;
                    #endregion

                    #region Adicionais
                    var lvwAdicionais = new ListView
                    {
                        HasUnevenRows = false,
                        SeparatorColor = Color.Transparent,
                        BackgroundColor = Color.Transparent,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        RowHeight = 60
                    };


                    if (item.Adicionais.Count == 0)
                    {
                        var adicionais = await App.Database.GetAdicionaisAsync(item.CodSecao, item.CodGrupo);

                        if (App.Parametro.OrdenarAdicionalDescricao)
                        {
                            adicionais.Sort((x, y) => string.Compare(x.Descricao, y.Descricao));
                        }
                        foreach (var adicional in adicionais)
                        {
                            var produto = await App.Database.GetProdutoAsync(adicional.Produto);
                            if (produto != null && !string.IsNullOrEmpty(produto.Descricao))
                            {

                                adicional.Valor = produto.Valor;
                                adicional.Descricao = produto.Descricao;
                                item.Adicionais.Add(adicional);
                            }
                        }
                    }

                    OnPropertyChange(nameof(item.Adicionais));
                    OnPropertyChange(nameof(item));

                    lvwAdicionais.ItemsSource = item.Adicionais;

                    var adicionaisTemplate = new DataTemplate(() =>
                    {
                        var grid = new Grid { Margin = new Thickness(0, 0, 0, 0), BackgroundColor = Color.Transparent, ColumnSpacing = 0, RowSpacing = 0 };
                        grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });

                        var lblProduto = new Label { Style = (Style)Application.Current.Resources["lblStyle"] };
                        lblProduto.SetBinding(Label.TextProperty, "Descricao");

                        var gridQuantidade = new Grid { Margin = new Thickness(0, 0, 0, 0), BackgroundColor = Color.Transparent, ColumnSpacing = 0, RowSpacing = 0 };

                        gridQuantidade.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });
                        gridQuantidade.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                        gridQuantidade.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Auto) });


                        TapGestureRecognizer tapDiminuir = new TapGestureRecognizer
                        {
                            NumberOfTapsRequired = 1

                        };


                        TapGestureRecognizer tapAumentar = new TapGestureRecognizer
                        {
                            NumberOfTapsRequired = 1
                        };


                        Binding objBindingTapDiminuir = new Binding()
                        {
                            Source = lvwAdicionais,
                            Path = "BindingContext.DiminuirQtdeCommand"
                        };

                        tapDiminuir.SetBinding(TapGestureRecognizer.CommandProperty, objBindingTapDiminuir);
                        tapDiminuir.SetBinding(TapGestureRecognizer.CommandParameterProperty, new Binding { Path = "." });

                        Binding objBindingTapAumentar = new Binding()
                        {
                            Source = lvwAdicionais,
                            Path = "BindingContext.AumentarQtdeCommand"
                        };

                        tapAumentar.SetBinding(TapGestureRecognizer.CommandProperty, objBindingTapAumentar);
                        tapAumentar.SetBinding(TapGestureRecognizer.CommandParameterProperty, new Binding { Path = "." });


                        var imgMenos = new CachedImage
                        {
                            Source = "BotaoMenos.png",
                            Style = (Style)Application.Current.Resources["ImgBtnList"],
                            HorizontalOptions = LayoutOptions.Start

                        };

                        var imgMais = new CachedImage
                        {
                            Source = "Adicionar.png",
                            Style = (Style)Application.Current.Resources["ImgBtnList"],
                            HorizontalOptions = LayoutOptions.End

                        };

                        imgMenos.GestureRecognizers.Add(tapDiminuir);
                        imgMais.GestureRecognizers.Add(tapAumentar);

                        var lblQuantidade = new Label
                        {
                            Style = (Style)Application.Current.Resources["lblStyle"],
                            TextColor = Color.FromHex("#f37021"),
                            HorizontalTextAlignment = TextAlignment.Center,
                            Margin = new Thickness(8, 0, 8, 0),
                        };

                        lblQuantidade.SetBinding(Label.TextProperty, "Quantidade");

                        gridQuantidade.Children.Add(imgMenos, 0, 0);
                        gridQuantidade.Children.Add(lblQuantidade, 1, 0);
                        gridQuantidade.Children.Add(imgMais, 2, 0);


                        grid.Children.Add(lblProduto, 0, 0);
                        grid.Children.Add(gridQuantidade, 1, 0);


                        return new ViewCell { View = grid };
                    });

                    lvwAdicionais.ItemTemplate = adicionaisTemplate;
                    #endregion

                    var lblAdicional = new Label
                    {
                        Text = "Adicionais",
                        Style = (Style)Application.Current.Resources["lblStyle"],
                        TextColor = (Color)Application.Current.Resources["AzulCardapio"]
                    };

                    var lblObservacaoPreCadastrada = new Label
                    {
                        Text = "Observações pré-cadastradas",
                        Style = (Style)Application.Current.Resources["lblStyle"],
                        TextColor = (Color)Application.Current.Resources["AzulCardapio"]
                    };

                    var entryObservacao = new Editor
                    {
                        Style = (Style)Application.Current.Resources["entryStyle"],
                        TextColor = (Color)Application.Current.Resources["AzulCardapio"],
                        BackgroundColor = (Color)Application.Current.Resources["FundoCinza"],
                        ClassId = Convert.ToString(item.Item),
                        Text = item.Observacao,
                        Keyboard = Keyboard.Default,
                        Margin = new Thickness(20, 0, 20, 10),
                    };

                    //Replicar Obs           
                    Command AplicarTodosObsCommand = new Command<string>(ExecuteAplicarTodosObsCommand);
                    var btnObs = new Button
                    {
                        BackgroundColor = Color.FromHex("#c3c6c9"),
                        TextColor = Color.FromHex("#f37021"),
                        Text = "Aplicar a Todos",
                        Command = AplicarTodosObsCommand,
                        CommandParameter = Convert.ToString(item.Item),
                        CornerRadius = 20,
                        Margin = new Thickness(0, 0, 20, 0)
                    };

                    var layoutObs = new StackLayout
                    {
                        BackgroundColor = Color.FromHex("#fff2ea"),
                        Orientation = StackOrientation.Horizontal,
                        ClassId = "StackOBS"
                    };

                    #region Pizza Sabores
                    if (item.QtdSaboresPizza > 0)
                    {
                        Command PizzaSaboresCommand = new Command<PedidoItem>(ExecutePizzaSaboresCommand);
                        TapGestureRecognizer command = new TapGestureRecognizer
                        {
                            Command = PizzaSaboresCommand,
                            CommandParameter = item,
                            NumberOfTapsRequired = 1
                        };

                        var btnMesa = new CachedImage { Source = "Pizza.png", BackgroundColor = Color.Transparent, Style = (Style)Application.Current.Resources["ImgBtn"], HorizontalOptions = LayoutOptions.End, HeightRequest = 60, WidthRequest = 60, Margin = new Thickness { Top = 5 }, ClassId = Convert.ToString(item.Item) };
                        btnMesa.GestureRecognizers.Add(command);


                        var layoutPizzaSabores = new StackLayout
                        {
                            BackgroundColor = Color.Transparent,
                            Orientation = StackOrientation.Horizontal,
                            ClassId = "StackPizzaSabores",
                            HorizontalOptions = LayoutOptions.End
                        };

                        layoutPizzaSabores.Children.Add(btnMesa);

                        layout.Children.Add(layoutPizzaSabores);
                    }
                    #endregion

                    layoutObs.Children.Add(entryObservacao);
                    layoutObs.Children.Add(btnObs);


                    layout.Children.Add(lblObservacaoPreCadastrada);
                    layout.Children.Add(lvwObservacoes);
                    layout.Children.Add(layoutObs);
                    layout.Children.Add(lblAdicional);
                    layout.Children.Add(lvwAdicionais);

                    Stks.Add(layout);
                }
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }

        }

        private async void ExecutePizzaSaboresCommand(PedidoItem item)
        {
            if (item.PizzasSabores.Count == 0)
            {
                var produtos = await App.Database.GetProdutosGrupo(item.CodSecao, item.CodGrupo);
                if (produtos != null)
                {
                    foreach (var produto in produtos)
                    {
                        if (produto.Codigo != item.Produto && produto.QtdeSaboresPizza > 0)
                        {
                            item.PizzasSabores.Add(produto);
                            OnPropertyChange(nameof(item.PizzasSabores));
                        }
                    }
                }
            }
            var page = new PizzaSaboresPage(item);
            await PopupNavigation.Instance.PushAsync(page);
        }

        private async void ExecuteCancelaCommand()
        {
            try
            {
                foreach (var stk in Stks)
                {
                    foreach (var i in stk.Children)
                    {

                        if (i.ClassId != null && i.ClassId != "")
                        {
                            if (i.ClassId == "StackOBS")
                            {
                                StackLayout stackObs = i as StackLayout;
                                foreach (var child in stackObs.Children)
                                {
                                    if (child.ClassId != null && child.ClassId != "")
                                    {
                                        Editor obs = child as Editor;
                                        var pedidoitem = PedidoItens.Where(p => Convert.ToString(p.Item) == child.ClassId).FirstOrDefault();
                                        if (pedidoitem != null && string.IsNullOrEmpty(obs.Text) == false)
                                            pedidoitem.Observacao = obs.Text + " ;";
                                    }
                                }
                            }
                        }
                    }
                }
                await App.Current.MainPage.Navigation.PopPopupAsync();
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGô", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }
        private async void ExecuteDiminuirQtdeCommand(Adicional adicional)
        {
            try
            {
                if ((adicional != null && adicional.Quantidade > 0))
                {
                    adicional.Quantidade = adicional.Quantidade - 1;
                }

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }

        private async void ExecuteAumentarQtdeCommand(Adicional adicional)
        {
            try
            {
                if (adicional != null)
                    adicional.Quantidade = adicional.Quantidade + 1;

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("AmiGo", e.Message, "OK");
                Crashes.TrackError(e);
            }
        }
    }
}
