﻿using AmiGo.Data;
using AmiGo.Helpers;
using AmiGo.Models;
using AmiGo.Services;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AmiGo
{
    public partial class App : Application
    {
        public static AmiGoDatabase database;
        public static Usuario Usuario { get; set; }
        public static Parametro Parametro { get; set; }
        public static Config Config { get; set; }
        public static string BaseUrl { get; set; }

        public App()
        {
            //LiveReload.Init();
            InitializeComponent();
            ConfiguraIp();

            if (Usuario != null && Usuario.Id > 0)
                MainPage = new NavigationPage(new Views.MainPage());
            else
                MainPage = new NavigationPage(new Views.LoginPage());

            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");

        }

        public static AmiGoDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new AmiGoDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("AmiGoSQLite.db3"));
                }
                return database;
            }
        }

        async void ConfiguraIp()
        {
            bool arquivoExiste = await PCLHelper.ArquivoExisteAsync("Config.txt");
            if (arquivoExiste != true)
            {
                await PCLHelper.CriarArquivo("Config.txt");
            }
            else
            {
                try
                {
                    Config = JsonConvert.DeserializeObject<Config>(await PCLHelper.ReadAllTextAsync("Config.txt"));
                    BaseUrl = Config.Host;
                }
                catch (Exception e)
                {
                    BaseUrl = "";
                    Crashes.TrackError(e);
                }
            }

            //BaseUrl = "http://192.168.100.55:2390/api/";
            //BaseUrl = "http://192.168.100.55:44353/api/";            
        }


        protected override void OnStart()
        {
            //AppCenter.Start("ios=7decf909-13dd-4993-a35d-24b095ddcbf5;" +
            //      "android=635d5c3c-2917-4061-8aeb-6a5a30e46633;",
            //      typeof(Analytics), typeof(Crashes));
            AppCenter.Start("ios=7decf909-13dd-4993-a35d-24b095ddcbf5;" +
                  "android=193203c5-1140-40cb-bef6-331b930e25d9;",
                  typeof(Analytics), typeof(Crashes));


        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
