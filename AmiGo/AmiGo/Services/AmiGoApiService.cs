﻿using AmiGo.Models;
using Microsoft.AppCenter.Crashes;
using ModernHttpClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(AmiGo.Services.AmiGoApiService))]
namespace AmiGo.Services
{
    public class AmiGoApiService : IAmiGoApiService
    {
        public async Task<List<Usuario>> GetUsuariosAsync()
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}Usuarios").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<Usuario>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<List<Produto>> GetProdutosAsync()
        {
            try
            {
                using (var httpClient = new HttpClient(new NativeMessageHandler()))
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                    if (App.Parametro.DataSincronizacao != null)
                    {
                        string date = Convert.ToDateTime(App.Parametro.DataSincronizacao).ToString("yyyy-MM-dd");
                        var response = await httpClient.GetAsync($"{App.BaseUrl}Produtos?Data={date}").ConfigureAwait(false);
                        if (response.IsSuccessStatusCode)
                        {
                            using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                            {
                                return JsonConvert.DeserializeObject<List<Produto>>(
                                    await new StreamReader(responseStream)
                                        .ReadToEndAsync().ConfigureAwait(false));
                            }
                        }
                        return null;
                    }
                    else
                    {
                        var response = await httpClient.GetAsync($"{App.BaseUrl}Produtos").ConfigureAwait(false);
                        if (response.IsSuccessStatusCode)
                        {
                            using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                            {
                                return JsonConvert.DeserializeObject<List<Produto>>(
                                    await new StreamReader(responseStream)
                                        .ReadToEndAsync().ConfigureAwait(false));
                            }
                        }
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return null;
            }

        }

        public async Task<List<Grupo>> GetGruposAsync()
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}Grupos").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<Grupo>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<List<ProdutoObservacao>> GetProdutoObservacaoAsync()
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}ProdutoObservacoes").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<ProdutoObservacao>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<List<Adicional>> GetAdicionaisAsync()
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}Adicionais").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<Adicional>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<List<Mesa>> GetMesasAsync()
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}Pedidos").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<Mesa>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<bool> PostPedidoAsync(ObservableCollection<PedidoItem> pedidoItens)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                    string result = JsonConvert.SerializeObject(pedidoItens);
                    var content = new StringContent(JsonConvert.SerializeObject(pedidoItens), Encoding.UTF8, "application/json");


                    var response = await httpClient.PostAsync($"{App.BaseUrl}Pedidos", content).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        return true;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    return true;
                }
            }
        }

        public async Task<List<Consumacao>> GetConsumacaoAsync(string mesa, string ficha)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                if (App.Parametro.Ficha)
                {
                    var response = await httpClient.GetAsync($"{App.BaseUrl}Pedidos/{mesa}?&Ficha={ficha}").ConfigureAwait(false);


                    if (response.IsSuccessStatusCode)
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        {
                            return JsonConvert.DeserializeObject<List<Consumacao>>(
                                await new StreamReader(responseStream)
                                    .ReadToEndAsync().ConfigureAwait(false));
                        }
                    }
                    return null;
                }
                else
                {
                    var response = await httpClient.GetAsync($"{App.BaseUrl}Pedidos/{mesa}").ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        {
                            return JsonConvert.DeserializeObject<List<Consumacao>>(
                                await new StreamReader(responseStream)
                                    .ReadToEndAsync().ConfigureAwait(false));
                        }
                    }
                    return null;
                }
            }
        }

        public async Task<Result> PutFechamentoAsync(PreVenda preVenda)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                string result = JsonConvert.SerializeObject(preVenda, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                var content = new StringContent(JsonConvert.SerializeObject(preVenda, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");
                var response = await httpClient.PutAsync($"{App.BaseUrl}Pedidos", content).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<Result>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }

                return new Result { Mensagem = "Erro Local" };
            }
        }

        public async Task<List<Ficha>> GetFichasMesaAsync(string mesa)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}Fichas/{mesa}").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<Ficha>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<Ficha> GetFichaAsync(string ficha)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}Fichas?&Ficha={ficha}").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<Ficha>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<Parametro> GetParametroAsync()
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}parametros/1").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<Parametro>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<bool> PutAlteraFichaAsync(Ficha ficha)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var content = new StringContent(JsonConvert.SerializeObject(ficha, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");
                var response = await httpClient.PutAsync($"{App.BaseUrl}Fichas", content).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                return false;
            }
        }

        //public async Task<bool> PutAlteraNomeFichaAsync(Ficha ficha)
        //{
        //    using (var httpClient = new HttpClient(new NativeMessageHandler()))
        //    {
        //        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

        //        //return await Policy.Handle<HttpRequestException>(ex => !ex.Message.ToLower().Contains("500")).WaitAndRetryAsync(retryCount: 3, sleepDurationProvider: retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), onRetry: (ex, time) => {/* UserDialogs.Instance.Toast($"Erro ao Buscar Atualização, tentando Novamente...");*/ }).ExecuteAsync(async () =>
        //        //{
        //        var content = new StringContent(JsonConvert.SerializeObject(ficha, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");                                
        //        var response = await httpClient.PostAsync($"{App.BaseUrl}Fichas", content).ConfigureAwait(false);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            return true;
        //        }
        //        return false;
        //        //});
        //    }
        //}

        public async Task<Result> PostJuntarFichasAsync(JuntaFicha juntaFicha)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var content = new StringContent(JsonConvert.SerializeObject(juntaFicha, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync($"{App.BaseUrl}Fichas", content).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<Result>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return new Result { Mensagem = "Erro Local" };
            }
        }

        public async Task<List<Finalizador>> GetFinalizadoresAsync(int maquina)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}Finalizadores/{maquina}").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<Finalizador>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<List<ProdutoExcluido>> GetProdutosExcluidosAsync(int? UltimoProdutoExcluido)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var response = await httpClient.GetAsync($"{App.BaseUrl}ProdutoExcluido/{UltimoProdutoExcluido}").ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<List<ProdutoExcluido>>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return null;
            }
        }

        public async Task<Result> PostPrinterFichaAsync(Ficha ficha)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var content = new StringContent(JsonConvert.SerializeObject(ficha, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync($"{App.BaseUrl}Print", content).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<Result>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return new Result { Mensagem = "Erro Local" };
            }
        }
        public async Task<Result> PostCancelarItemAsync(PedidoItem pedidoItem)
        {
            using (var httpClient = new HttpClient(new NativeMessageHandler()))
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.Timeout = TimeSpan.FromMilliseconds(10000);

                var content = new StringContent(JsonConvert.SerializeObject(pedidoItem, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), Encoding.UTF8, "application/json");
                var response = await httpClient.PostAsync($"{App.BaseUrl}CancelarItem", content).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    {
                        return JsonConvert.DeserializeObject<Result>(
                            await new StreamReader(responseStream)
                                .ReadToEndAsync().ConfigureAwait(false));
                    }
                }
                return new Result { Mensagem = "Erro Local" };
            }
        }

    }
}
