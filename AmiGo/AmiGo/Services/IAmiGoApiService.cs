﻿using AmiGo.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace AmiGo.Services
{
    public interface IAmiGoApiService
    {
        Task<List<Usuario>> GetUsuariosAsync();
        Task<List<Produto>> GetProdutosAsync();
        Task<List<Grupo>> GetGruposAsync();
        Task<List<ProdutoObservacao>> GetProdutoObservacaoAsync();
        Task<List<Adicional>> GetAdicionaisAsync();
        Task<bool> PostPedidoAsync(ObservableCollection<PedidoItem> pedidoItens);
        Task<List<Mesa>> GetMesasAsync();
        Task<List<Consumacao>> GetConsumacaoAsync(string mesa, string ficha);
        Task<Result> PutFechamentoAsync(PreVenda preVenda);
        Task<List<Ficha>> GetFichasMesaAsync(string mesa);
        Task<Ficha> GetFichaAsync(string ficha);
        Task<Parametro> GetParametroAsync();
        Task<bool> PutAlteraFichaAsync(Ficha ficha);
        Task<Result> PostJuntarFichasAsync(JuntaFicha juntaFicha);
        Task<List<Finalizador>> GetFinalizadoresAsync(int maquina);
        Task<List<ProdutoExcluido>> GetProdutosExcluidosAsync(int? UltimoProdutoExcluido);
        Task<Result> PostPrinterFichaAsync(Ficha ficha);
        Task<Result> PostCancelarItemAsync(PedidoItem pedidoItem);
    }
}
