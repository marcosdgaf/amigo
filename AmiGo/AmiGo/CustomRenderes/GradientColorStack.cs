﻿using Xamarin.Forms;

namespace AmiGo.CustomRenderes
{
    public class GradientColorStack : StackLayout
    {
        public Color StartColor { get; set; }
        public Color EndColor { get; set; }
        public bool Vertical { get; set; }
    }
}
