﻿
using Xamarin.Forms;

namespace AmiGo.CustomRenderes
{
    public class CustomEntry : Entry
    {
        public CustomEntry()
        {
            this.TextChanged += (sender, e) =>
            {
                this.InvalidateMeasure();
            };
        }
    }
}
