﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class JuntaFicha
    {
        public int NovaFicha { get; set; }
        public int Usuario { get; set; }
        public int? Vendedor { get; set; }
        public List<Ficha> Fichas { get; set; }

        public JuntaFicha()
        {
            Fichas = new List<Ficha>();
        }
    }
}
