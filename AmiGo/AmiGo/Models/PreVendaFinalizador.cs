﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class PreVendaFinalizador
    {
        public int? Maquina { get; set; }
        public DateTime? Data { get; set; }
        public int? Controle { get; set; }
        public int Finalizador { get; set; }
        public decimal? Valor { get; set; }
        public decimal? Troco { get; set; }
    }
}
