﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class Consumacao
    {
        public int? Produto { get; set; }
        public double Quantidade { get; set; }
        public decimal Valor { get; set; }
        public DateTime? Hora { get; set; }
    }
}
