﻿using System.ComponentModel;
using System.Xml.Serialization;
using System;
using System.Globalization;
using AmiGo.ViewModels;

namespace AmiGo.Models
{
    public class Finalizador :BaseViewModel
    {
        public int Codigo { get; set; }
        public int Maquina { get; set; }
        public string Descricao { get; set; }
        public int? Funcao { get; set; }
        public int? MovimentoReceber { get; set; }
        public string Inativo { get; set; }
        public string NaoGeraTroco { get; set; }
        public string ComprovanteNaoFiscal { get; set; }
        public string MovimentarCaixa { get; set; }
        public int? TipoFinalizador { get; set; }        
        
        private decimal? _valor;        
        public decimal? Valor
        {
            get => _valor;
            set
            {
                SetProperty(ref _valor, value);
                OnPropertyChange(nameof(Valor));                                
            }
        }

        private decimal? _troco;
        public decimal? Troco
        {
            get => _troco;
            set
            {
                SetProperty(ref _troco, value);
                OnPropertyChange(nameof(Troco));
            }
        }

    }    
}
