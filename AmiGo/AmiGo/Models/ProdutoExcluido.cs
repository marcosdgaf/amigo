﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class ProdutoExcluido
    {
        public int Id { get; set; }
        public int? Produto { get; set; }
        public DateTime? Data { get; set; }
    }
}
