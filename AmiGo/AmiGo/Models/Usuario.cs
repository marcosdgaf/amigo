﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class Usuario
    {
        [PrimaryKey]
		public int Vendedor { get; set; }
        public int Id { get; set; }
        public string Nome { get; set; }        
        public string Login { get; set; }
        public string Senha { get; set; }
        public string NomeVendedor { get; set; }

    }
}
