﻿using AmiGo.ViewModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace AmiGo.Models
{
    public class Pedido : BaseViewModel
    {
        [PrimaryKey, AutoIncrement, Unique]
        public int id { get; set; }
        public int Mesa { get; set; }
        public DateTime Data { get; set; }
        public int Status { get; set; }
        public string Nome { get; set; }


        private ObservableCollection<PedidoItem> _pedidoItens;
        [Ignore]
        public ObservableCollection<PedidoItem> PedidoItens
        {
            get { return _pedidoItens; }
            set
            {
                SetProperty(ref _pedidoItens, value);
                OnPropertyChange(nameof(PedidoItens));
            }
        }

        //status 1=Aberto, 2=Finalizado
        public Pedido()
        {
            Status = 1;
        }
    }
}
