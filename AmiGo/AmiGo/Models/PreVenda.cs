﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class PreVenda
    {
        public int? Maquina { get; set; }
        public DateTime? Data { get; set; }
        public DateTime? Hora { get; set; }
        public int? Controle { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal? Desconto { get; set; }
        public decimal? Acrescimo { get; set; }
        public decimal? TotalVenda { get; set; }
        public int? Cliente { get; set; }
        public int? Vendedor { get; set; }
        public string Status { get; set; }
        public int? MaquinaCupom { get; set; }
        public DateTime? DataCupom { get; set; }
        public int? ControleCupom { get; set; }
        public string Justificativa { get; set; }
        public int? Fechamento { get; set; }
        public int? Usuario { get; set; }
        public DateTime? HoraFechamento { get; set; }
        public int? MaquinaFechamento { get; set; }
        public int? UsuarioFinalizador { get; set; }
        public int? IdFicha { get; set; }
        //public List<PreVendaItem> Items { get; set; }
        public string AdicionadoServico { get; set; }
        public decimal? ValorCouvert { get; set; }
        public int? QtdePessoas { get; set; }
        public decimal? ValorPessoa { get; set; }
        public string Observacao { get; set; }
        public int? Ficha { get; set; }        
        public int? Mesa { get; set; }
        public decimal? PorcentagemServico { get; set; }
        public string CpfCnpj { get; set; }
        public bool EmitirFiscal { get; set; }
        public List<Finalizador> Finalizadores { get; set; }


        public PreVenda()
        {
            Finalizadores = new List<Finalizador>();
        }
    }
}
