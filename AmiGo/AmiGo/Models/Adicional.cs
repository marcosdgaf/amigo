﻿using AmiGo.ViewModels;
using SQLite;

namespace AmiGo.Models
{
    public class Adicional : BaseViewModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Codigo { get; set; }
        public int Produto { get; set; }
        public int? Secao { get; set; }
        public int? Grupo { get; set; }
        public int? Subgrupo { get; set; }

        [Ignore]
        public string Descricao { get; set; }

        [Ignore]
        public double? Valor { get; set; }

        private int _quantidade;
        [Ignore]
        public int Quantidade
        {
            get => _quantidade;
            set
            {
                SetProperty(ref _quantidade, value);
                OnPropertyChange(nameof(Quantidade));
            }
        }

    }
}
