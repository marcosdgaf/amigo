﻿using AmiGo.ViewModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class ProdutoObservacao : BaseViewModel
    {
        [PrimaryKey]
        public int Codigo { get; set; }
        public int Secao { get; set; }
        public int? Grupo { get; set; }
        public int? Produto { get; set; }
        public string Observacao { get; set; }
        //public int? Mesa { get; set; }
        //public int? Item { get; set; }
        //public Boolean Incluir { get; set; }

        private int _quantidade;
        [Ignore]
        public int Quantidade
        {
            get { return _quantidade; }
            set
            {
                SetProperty(ref _quantidade, value);
                OnPropertyChange(nameof(Quantidade));
            }
        }
    }
}
