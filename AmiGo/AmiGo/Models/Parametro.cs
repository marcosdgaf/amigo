﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class Parametro
    {
        [PrimaryKey]
        public int Id { get; set; }
        [JsonIgnore]
        public DateTime DataInstalacao { get; set; }
        [JsonIgnore]
        public DateTime DataSincronizacao { get; set; }
        //public int VersaoBd { get; set; }
        [JsonIgnore]
        public bool? SalvarUsuario { get; set; }
        [JsonIgnore]
        public string Login { get; set; }
        [JsonIgnore]
        public string Senha { get; set; }
        [JsonIgnore]
        public int TabMesas { get; set; }
        [JsonIgnore]
        public bool PesquisaDescricao { get; set; }
        [JsonIgnore]
        public bool OrdenarDescricao { get; set; }
        [JsonIgnore]
        public bool OrdenarAdicionalDescricao { get; set; }
        [JsonIgnore]
        public bool PesquisarPorFicha { get; set; }
        public bool Ficha { get; set; }
        public decimal Couvert { get; set; }
        public decimal MinGarcom { get; set; }
        public decimal MaxGarcom { get; set; }
        public string ServidorImpressao { get; set; }
        public string MostrarHoraPedido { get; set; }
        public string MostrarPrecoCardapio { get; set; }
        public int? UltimoProdutoExcluido { get; set; }
        public string BloquearFechamento { get; set; }
        public string BloquearAberturaFicha { get; set; }
        public string PermitirCancelarItemFicha { get; set; }
        [JsonIgnore]
        public int TelaInicialPadao { get; set; }
        [JsonIgnore]
        public bool TelaInicialFichasVisible { get; set; }     
    }
}
