﻿using AmiGo.ViewModels;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace AmiGo.Models
{
    public class Produto : BaseViewModel
    {        
        [PrimaryKey]
        public int Codigo { get; set; }
        [Ignore]
        [JsonIgnore]
        public Grupo Grupo { get; set; }
        public int? Tipo { get; set; }        
        public string Descricao { get; set; }
        public double? Valor { get; set; }
        public string Inativo { get; set; }
        public double? ValorPromocional { get; set; }
        public string AbrirAdicionais { get; set; }
        public string Adicional { get; set; }
        public TimeSpan? HoraInicioPromocao { get; set; }
        public TimeSpan? HoraFimPromocao { get; set; }
        public DateTime? DataInicioPromocao { get; set; }
        public DateTime? DataFimPromocao { get; set; }
        public int? CodGrupo { get; set; }
        public int? CodSecao { get; set; }
        [JsonIgnore][Ignore]
        public double Consumido { get; set; }
        [JsonIgnore][Ignore]
        public DateTime? Hora { get; set; }
        [JsonIgnore][Ignore]
        public bool VisualizarPreco { get; set; }

        [JsonIgnore]        
        private bool _selecionado;
        [JsonIgnore]
        [Ignore]
        public bool Selecionado
        {
            get { return _selecionado; }
            set
            {
                SetProperty(ref _selecionado, value);
                OnPropertyChange(nameof(Selecionado));
            }
        }
        //public Pedido Pedido { get; set; }

        private double _quantidade;
        public double Quantidade
        {
            get { return _quantidade; }
            set
            {
                SetProperty(ref _quantidade, value);
                OnPropertyChange(nameof(Quantidade));
            }
        }

        public int? QtdeSaboresPizza { get; set; }
        //public event PropertyChangedEventHandler PropertyChanged;

        //protected virtual void OnPropertyChange([CallerMemberName]string propertyName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}

        //protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        //{
        //    if (EqualityComparer<T>.Default.Equals(storage, value))
        //    {
        //        return false;
        //    }

        //    storage = value;
        //    OnPropertyChange(propertyName);
        //    return true;
        //}


    }
}
