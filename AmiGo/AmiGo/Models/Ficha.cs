﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class Ficha
    {
        public int Id { get; set; }
        public int Numero { get; set; }
        public int? Mesa { get; set; }
        public string Nome { get; set; }
        public string Status { get; set; }
        public DateTime Data { get; set; }
        public int? Cliente { get; set; }
        public int? Vendedor { get; set; }
        public bool Bloqueada { get; set; }
        public DateTime? DataFinalizacao { get; set; }
        public bool Selecionado { get; set; }
        public int? PessoasCouvert { get; set; }
        public decimal? ValorCouvert { get; set; }
        public decimal? porcentagemServico { get; set; }

    }
}
