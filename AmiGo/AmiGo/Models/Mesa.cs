﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class Mesa
    {
        public int Numero { get; set; }
        public string Imagem { get; set; }
        public string Status { get; set; }
    }
}
