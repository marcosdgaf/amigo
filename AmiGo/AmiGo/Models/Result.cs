﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmiGo.Models
{
    public class Result
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
    }
}
