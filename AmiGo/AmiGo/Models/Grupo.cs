﻿using SQLite;

namespace AmiGo.Models
{
    public class Grupo
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public int Codigo { get; set; }
        public int Secao { get; set; }
        public string Descricao { get; set; }
        public string DividirSaboresPizza { get; set; }
    }
}
