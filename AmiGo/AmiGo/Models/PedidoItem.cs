﻿using AmiGo.ViewModels;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace AmiGo.Models
{
    public class PedidoItem : BaseViewModel
    {        
        public int Item { get; set; }
        public int Produto { get; set; }
        public int? CodGrupo { get; set; }
        public int? CodSecao { get; set; }
        public string Mesa { get; set; }
        public string Ficha { get; set; }
        public string Descricao { get; set; }
        public string Nome { get; set; }
        public DateTime Data { get; set; }
        public double? Valor { get; set; }
        public double? PorGarcom { get; set; }
        public int? SaboresPizza { get; set; }
        public int? QtdSaboresPizza { get; set; }
        //public string Observacao { get; set; }
        public int? Maquina { get; set; }
        [Ignore]
        public int? Vendedor { get; set; }

        private ObservableCollection<ProdutoObservacao> _produtoObservacao;
        [Ignore][JsonIgnore]
        public ObservableCollection<ProdutoObservacao> ProdutoObservacao
        {
            get { return _produtoObservacao; }
            set
            {
                SetProperty(ref _produtoObservacao, value);
                OnPropertyChange(nameof(ProdutoObservacao));
            }
        }

        private ObservableCollection<Adicional> _adicionais;
        [Ignore]
        public ObservableCollection<Adicional> Adicionais
        {
            get { return _adicionais; }
            set
            {
                SetProperty(ref _adicionais, value);
                OnPropertyChange(nameof(Adicionais));
            }
        }        
        [JsonIgnore]
        private ObservableCollection<Produto> _pizzasSabores;
        [Ignore]
        [JsonIgnore]
        public ObservableCollection<Produto> PizzasSabores
        {
            get { return _pizzasSabores; }
            set
            {
                SetProperty(ref _pizzasSabores, value);
                OnPropertyChange(nameof(PizzasSabores));
            }
        }
        
        private int _quantidade;
        public int Quantidade
        {
            get { return _quantidade; }
            set
            {
                SetProperty(ref _quantidade, value);
                OnPropertyChange(nameof(Quantidade));
            }
        }


        private string _observacao;
        public string Observacao
        {
            get { return _observacao; }
            set
            {
                SetProperty(ref _observacao, value);
                OnPropertyChange(nameof(Observacao));
            }
        }


        public PedidoItem()
        {
            ProdutoObservacao = new ObservableCollection<ProdutoObservacao>();
            Adicionais = new ObservableCollection<Adicional>();
            PizzasSabores = new ObservableCollection<Produto>();
            Data = DateTime.Now;
        }

    }
}
