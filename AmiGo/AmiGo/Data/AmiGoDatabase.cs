﻿using AmiGo.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AmiGo.Data
{
    public class AmiGoDatabase
    {
        readonly SQLiteAsyncConnection database;
        public string Path { get; set; }

        public AmiGoDatabase(string dbPath)
        {
            Path = dbPath;
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Usuario>().Wait();
            database.CreateTableAsync<Parametro>().Wait();
            database.CreateTableAsync<Grupo>().Wait();
            database.CreateTableAsync<Produto>().Wait();
            database.CreateTableAsync<ProdutoObservacao>().Wait();
            database.CreateTableAsync<Adicional>().Wait();

        }


        public void ApagaDados()
        {

            database.DropTableAsync<Adicional>().Wait();
            database.DropTableAsync<Usuario>().Wait();
            database.DropTableAsync<Parametro>().Wait();
            database.DropTableAsync<Grupo>().Wait();
            database.DropTableAsync<Produto>().Wait();
            database.DropTableAsync<ProdutoObservacao>().Wait();
            database.DropTableAsync<Adicional>().Wait();
            database.CreateTableAsync<Adicional>().Wait();
            database.CreateTableAsync<Usuario>().Wait();
            database.CreateTableAsync<Parametro>().Wait();
            database.CreateTableAsync<Grupo>().Wait();
            database.CreateTableAsync<Produto>().Wait();
            database.CreateTableAsync<ProdutoObservacao>().Wait();
            database.CreateTableAsync<Adicional>().Wait();            
        }

        public void ApagaAdicionais()
        {
            database.DropTableAsync<Adicional>().Wait();
            database.CreateTableAsync<Adicional>().Wait();
        }
        #region Produtos

        public Task<List<Produto>> GetProdutosGrupo(int? secao, int? grupo)
        {
            return database.Table<Produto>().Where(p => p.CodSecao == secao && p.CodGrupo==grupo).ToListAsync();
        }
        public Task<List<Produto>> GetProdutosAsync(int Tipo)
        {
            if (Tipo == 3)
            {
                if (App.Parametro.OrdenarDescricao)
                    return database.Table<Produto>().Where(p => p.Inativo != "S").OrderBy(o => o.Descricao).ToListAsync();
                else
                    return database.Table<Produto>().Where(p => p.Inativo != "S").OrderBy(o => o.Codigo).ToListAsync();

            }
            else
            {
                if (App.Parametro.OrdenarDescricao)
                    return database.Table<Produto>().Where(p => p.Tipo == Tipo && p.Inativo != "S").OrderBy(o => o.Descricao).ToListAsync();
                else
                    return database.Table<Produto>().Where(p => p.Tipo == Tipo && p.Inativo != "S").OrderBy(o => o.Codigo).ToListAsync();
            }

        }

        public Task<Produto> GetProdutoAsync(int? codigo)
        {
            return database.Table<Produto>().Where(p => p.Codigo == codigo).FirstOrDefaultAsync();
        }
        //public Task<List<Produto>> GetProdutosAsync(int Tipo)
        //{
        //    return database.QueryAsync<Produto>("SELECT * FROM [Produto] P LEFT JOIN ProdutoObservacao O ON P.CodGrupo=O.Grupo FROM  WHERE [P.Tipo] = 0");
        //}

        public Task<List<Produto>> GetProdutosNotDoneAsync()
        {
            return database.QueryAsync<Produto>("SELECT * FROM [Produto] WHERE [Id] = 0");
        }

        public Task<int> SaveProdutosAsync(Produto produto)
        {
            return database.InsertOrReplaceAsync(produto);
        }

        public Task<int> DeleteProdutosAsync(Produto produto)
        {
            return database.DeleteAsync(produto);
        }
        #endregion

        #region Usuarios
        public Task<int> SaveUsuarioAsync(Usuario usuario)
        {
            return database.InsertOrReplaceAsync(usuario);
        }


        public Task<List<Usuario>> GetUsuariosAsync()
        {
            return database.Table<Usuario>().ToListAsync();
        }

        public Task<Usuario> GetUsuarioAsync(string login, string senha)
        {
            return database.Table<Usuario>().Where(i => i.Login == login && i.Senha == senha).FirstOrDefaultAsync();
        }
        #endregion

        #region Parametros
        public Task<Parametro> GetParametroAsync(int id)
        {
            return database.Table<Parametro>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveParametroAsync(Parametro parametro)
        {
            return database.InsertOrReplaceAsync(parametro);
        }
        #endregion

        #region Grupos
        public Task<int> SaveGruposAsync(Grupo grupo)
        {
            return database.InsertOrReplaceAsync(grupo);
        }

        public Task<List<Grupo>> GetGrupoAsync()
        {
            //if (App.Parametro.OrdenarDescricao)
            //    return database.Table<Grupo>().OrderBy(o => o.Descricao).ToListAsync();
            //else
            //    return database.Table<Grupo>().OrderBy(o => o.Codigo).ToListAsync();

            return database.Table<Grupo>().ToListAsync();

        }

        public Task<Grupo> GetGrupoSecaoAsync(int secao, int codigo)
        {
            //if (App.Parametro.OrdenarDescricao)
            //    return database.Table<Grupo>().OrderBy(o => o.Descricao).ToListAsync();
            //else
            //    return database.Table<Grupo>().OrderBy(o => o.Codigo).ToListAsync();

            return database.Table<Grupo>().Where(p=>p.Secao==secao && p.Codigo==codigo).FirstOrDefaultAsync();

        }
        #endregion


        #region ProdutosObservacoes
        public Task<int> SaveObservacaoAsync(ProdutoObservacao observacao)
        {
            return database.InsertOrReplaceAsync(observacao);
        }

        public Task<List<ProdutoObservacao>> GetObservacaoAsync(int? secao, int? grupo, int? produto)
        {
            return database.Table<ProdutoObservacao>().Where(o => (o.Grupo == grupo && o.Secao == secao && o.Produto == null) || (o.Produto == produto)).ToListAsync();
        }
        #endregion


        #region ProdutosAdicionais
        public Task<int> SaveAdicionalAsync(Adicional adicional)
        {
            return database.InsertAsync(adicional);
        }

        public Task<List<Adicional>> GetAdicionaisAsync(int? secao, int? grupo)
        {
            return database.Table<Adicional>().Where(o => (o.Grupo == grupo && o.Secao == secao)).ToListAsync();
            //return database.Table<Adicional>().ToListAsync();
        }

        public Task<List<Adicional>> GetAdicionalAsync(int? codigo, int? produto)
        {
            return database.Table<Adicional>().Where(o => (o.Codigo == codigo && o.Produto == produto)).ToListAsync();
        }
        #endregion

    }
}
