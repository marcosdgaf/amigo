﻿using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FichaPage : PopupPage
    {
        public string Mesa { get; set; }
        public FichaPage(string mesa)
        {
            InitializeComponent();
            BindingContext = new FichaViewModel(mesa);

            MessagingCenter.Subscribe<App>((App)App.Current, "Atualizar", (sender) =>
            {
                CarregaFichas(mesa);
            });
        }

        void CarregaFichas(string mesa)
        {
            var vm = BindingContext as FichaViewModel;
            vm.CarregaFichas(mesa);
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<MainPage>(this, "Atualizar");
            base.OnDisappearing();
        }

        private void ImageButton_Clicked(object sender, EventArgs e)
        {
            if (gridNovaFicha.IsVisible == false)
            {
                gridNovaFicha.IsVisible = true;
                btnAdicionar.Source = "FecharCancelar.png";
            }
            else
            {
                gridNovaFicha.IsVisible = false;
                btnAdicionar.Source = "Adicionar.png";
                entryFicha.Text = "";
                entryNome.Text = "";
            }
        }
    }
}