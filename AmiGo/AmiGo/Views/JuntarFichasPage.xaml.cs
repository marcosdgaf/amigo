﻿using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JuntarFichasPage : PopupPage
    {
        public JuntarFichasPage(string mesa)
        {
            InitializeComponent();
            BindingContext = new JuntarFichasViewModel(mesa);
        }

        private void BarraPesquisa_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var vm = BindingContext as JuntarFichasViewModel;
                LvwFichas.ItemsSource = vm.Fichas;

                if (e.NewTextValue != null && e.NewTextValue != "")
                {
                    LvwFichas.ItemsSource = vm.Fichas.Where(x => Convert.ToString(x.Numero).ToUpper().Contains(vm.Pesquisa.ToUpper()) ||
                    (x.Nome ?? "").ToUpper().Contains(vm.Pesquisa.ToUpper()));
                }
            }
            catch (Exception ex)
            {
                App.Current.MainPage.DisplayAlert("Amigo", "Erro: " + ex.Message, "OK");
            }
        }
    }
}