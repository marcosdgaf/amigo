﻿using AmiGo.Models;
using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FinalizadoresPage : PopupPage
    {        
        public FinalizadoresPage(string mesa, string ficha, string nome)
        {
            InitializeComponent();
            BindingContext = new FinalizadoresViewModel(mesa, ficha, nome);            
        }
    }
}