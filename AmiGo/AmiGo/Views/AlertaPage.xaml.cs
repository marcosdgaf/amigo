﻿using AmiGo.Models;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlertaPage : PopupPage
    {
        public  Result result;
        public AlertaPage(Result result)
        {
            InitializeComponent();
            this.result = result;
        }
    }
}