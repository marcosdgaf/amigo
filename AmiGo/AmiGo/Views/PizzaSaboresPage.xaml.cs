﻿using AmiGo.Models;
using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PizzaSaboresPage : PopupPage
    {
        public PizzaSaboresPage(PedidoItem item)
        {
            InitializeComponent();
            BindingContext = new PizzaSaboresViewModel(item);
        }

        private void Switch_Toggled(object sender, ToggledEventArgs e)
        {
            var produto = (sender as Switch).BindingContext as Produto;

            if (produto != null)
            {
                var vm = BindingContext as PizzaSaboresViewModel;

                vm.AdicionaProduto(produto);
                //if (produto.Selecionado)
                //    vm.AdicionaProduto(produto);
                //else
                //    vm.RemoveProduto(produto);

                base.OnAppearing();
            }
        }
    }
}