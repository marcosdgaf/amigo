﻿using AmiGo.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            BindingContext = new LoginViewModel();
        }

        private void EntryLogin_Completed(object sender, System.EventArgs e)
        {
            EntrySenha.Focus();
        }

        private void EntrySenha_Completed(object sender, System.EventArgs e)
        {
            BtnLogin.Command.Execute(sender);
        }

        private void BtnShowPass_Clicked(object sender, System.EventArgs e)
        {
            if ((BtnShowPass.Source as FileImageSource).File == "ShowPass.png")
            {
                BtnShowPass.Source = "HidePass.png";
                EntrySenha.IsPassword = false;
            }
            else
            {
                BtnShowPass.Source = "ShowPass.png";
                EntrySenha.IsPassword = true;
            }

        }
    }
}