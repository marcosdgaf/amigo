﻿using AmiGo.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using System;
using System.Linq;
using AmiGo.Models;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainViewModel();

            switch (App.Parametro.TelaInicialPadao)
            {
                case 0:
                    _clickA();
                    break;
                case 1:
                    _clickB();
                    break;
                default:
                    _clickA();
                    break;
            }

            MessagingCenter.Subscribe<App>((App)App.Current, "AtualizarTela", (sender) =>
            {
                _imgButtonClick();
            });
        }

        void Handle_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            //Debug.WriteLine("Posição " + e.NewValue + " Selecionada.");
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<MainPage>(this, "AtualizarTela");
            base.OnDisappearing();
        }


        void Handle_Scrolled(object sender, CarouselView.FormsPlugin.Abstractions.ScrolledEventArgs e)
        {
            //Debug.WriteLine("Scrolled to " + e.NewValue + " percent.");
            //Debug.WriteLine("Direction = " + e.Direction);
        }

        private void BtnA_Clicked(object sender, EventArgs e)
        {
            _clickA();
        }
        private void _clickA()
        {
            Mesas.BackgroundColor = Color.Transparent;
            Mesas.Source = "mesaON.png";
            Fichas.BackgroundColor = Color.LightGray;
            Fichas.Source = "fichaOFF.png";

            listMesas.IsVisible = true;
            listFichas.IsVisible = false;
        }

        private void BtnB_Clicked(object sender, EventArgs e)
        {
            _clickB();
        }
        private void _clickB()
        {
            Fichas.BackgroundColor = Color.Transparent;
            Fichas.Source = "fichaON.png";
            Mesas.BackgroundColor = Color.LightGray;
            Mesas.Source = "mesaOFF.png";
            listMesas.IsVisible = false;
            listFichas.IsVisible = true;
            var vm = BindingContext as MainViewModel;
            vm.CarregaFichas();
        }

        private void BarraPesquisa_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var vm = BindingContext as MainViewModel;
                LvwFichas.ItemsSource = vm.Fichas;

                if (e.NewTextValue != null && e.NewTextValue != "")
                {
                    LvwFichas.ItemsSource = vm.Fichas.Where(x => Convert.ToString(x.Numero).ToUpper().Contains(vm.PesquisaFicha.ToUpper()) ||
                    (x.Nome ?? "").ToUpper().Contains(vm.PesquisaFicha.ToUpper()));
                }
            }
            catch (Exception ex)
            {
                App.Current.MainPage.DisplayAlert("Amigo", "Erro: " + ex.Message, "OK");
            }
        }

        private void ImageButton_Clicked(object sender, EventArgs e)
        {
            _imgButtonClick();
        }
        private void _imgButtonClick()
        {
            if (gridNovaFicha.IsVisible == false)
            {
                gridNovaFicha.IsVisible = true;
                btnAdicionar.Source = "FecharCancelar.png";
            }
            else
            {
                gridNovaFicha.IsVisible = false;
                btnAdicionar.Source = "Adicionar.png";
                entryFicha.Text = "";
                entryNome.Text = "";
                entryMesa.Text = "";
            }
        }
    }
}

