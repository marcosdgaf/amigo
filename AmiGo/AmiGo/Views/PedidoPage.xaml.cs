﻿using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PedidoPage : PopupPage
    {
        public PedidoPage(string mesa, string ficha, string nome)
        {
            InitializeComponent();
            BindingContext = new PedidoViewModel(mesa, ficha, nome);

            if (App.Parametro.PesquisaDescricao != true)
            {
                ListCardapio.IsVisible = true;
                ListBebidas.IsVisible = false;
            }
            else
            {
                ListCardapioDescricao.IsVisible = true;
                ListBebidasDescricao.IsVisible = false;
                BarraPesquisa.IsVisible = true;
            }

            MessagingCenter.Subscribe<App>((App)App.Current, "Atualizar", (sender) =>
            {
                CarregarConsumo(mesa, ficha);
            });
        }

        void CarregarConsumo(string mesa, string ficha)
        {
            var vm = BindingContext as PedidoViewModel;
            vm.VerificaConsumacao(mesa, ficha);
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<MainPage>(this, "Atualizar");
            base.OnDisappearing();
        }

        private void BtnA_Clicked(object sender, EventArgs e)
        {
            Cardapio.BackgroundColor = Color.Transparent;
            Bebidas.BackgroundColor = Color.Silver;
            Inseridos.BackgroundColor = Color.Silver;
            Consumacao.BackgroundColor = Color.Silver;

            Cardapio.Source = "CardapioON.png";
            Bebidas.Source = "BebidasOFF.png";
            Inseridos.Source = "ChecagemOFF.png";
            Consumacao.Source = "FecharMesaOFF.png";

            if (App.Parametro.PesquisaDescricao != true)
            {
                ListCardapio.IsVisible = true;
                ListBebidas.IsVisible = false;
            }
            else
            {
                ListCardapioDescricao.IsVisible = true;
                ListBebidasDescricao.IsVisible = false;
                BarraPesquisa.IsVisible = true;
            }

            ListItensInseridos.IsVisible = false;
            stackConsumacao.IsVisible = false;

        }

        private void BtnB_Clicked(object sender, EventArgs e)
        {
            Cardapio.BackgroundColor = Color.Silver;
            Bebidas.BackgroundColor = Color.Transparent;
            Inseridos.BackgroundColor = Color.Silver;
            Consumacao.BackgroundColor = Color.Silver;

            Cardapio.Source = "CardapioOFF.png";
            Bebidas.Source = "BebidasON.png";
            Inseridos.Source = "ChecagemOFF.png";
            Consumacao.Source = "FecharMesaOFF.png";

            if (App.Parametro.PesquisaDescricao != true)
            {
                ListCardapio.IsVisible = false;
                ListBebidas.IsVisible = true;
            }
            else
            {
                ListCardapioDescricao.IsVisible = false;
                ListBebidasDescricao.IsVisible = true;
                BarraPesquisa.IsVisible = true;
            }

            ListItensInseridos.IsVisible = false;
            stackConsumacao.IsVisible = false;

        }

        private void BtnC_Clicked(object sender, EventArgs e)
        {
            Cardapio.BackgroundColor = Color.Silver;
            Bebidas.BackgroundColor = Color.Silver;
            Inseridos.BackgroundColor = Color.Transparent;
            Consumacao.BackgroundColor = Color.Silver;

            Cardapio.Source = "CardapioOFF.png";
            Bebidas.Source = "BebidasOFF.png";
            Inseridos.Source = "ChecagemON.png";
            Consumacao.Source = "FecharMesaOFF.png";

            ListCardapio.IsVisible = false;
            ListBebidas.IsVisible = false;
            ListItensInseridos.IsVisible = true;
            stackConsumacao.IsVisible = false;
            ListCardapioDescricao.IsVisible = false;
            ListBebidasDescricao.IsVisible = false;
            BarraPesquisa.IsVisible = false;

            var Vm = BindingContext as PedidoViewModel;
            Vm.CarregaItensPedidos();
        }

        private void BtnD_Clicked(object sender, EventArgs e)
        {
            Cardapio.BackgroundColor = Color.Silver;
            Bebidas.BackgroundColor = Color.Silver;
            Inseridos.BackgroundColor = Color.Silver;
            Consumacao.BackgroundColor = Color.Transparent;

            Cardapio.Source = "CardapioOFF.png";
            Bebidas.Source = "BebidasOFF.png";
            Inseridos.Source = "ChecagemOFF.png";
            Consumacao.Source = "FecharMesaON.png";

            ListCardapio.IsVisible = false;
            ListBebidas.IsVisible = false;
            ListItensInseridos.IsVisible = false;
            stackConsumacao.IsVisible = true;
            ListCardapioDescricao.IsVisible = false;
            ListBebidasDescricao.IsVisible = false;
            BarraPesquisa.IsVisible = false;
        }

        private void BarraPesquisa_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var vm = BindingContext as PedidoViewModel;
                if (e.NewTextValue != null && e.NewTextValue != "")
                {
                    ListCardapioDescricao.ItemsSource = vm.Cardapio.Where(x => x.Descricao.ToUpper().Contains(vm.Pesquisa.ToUpper()) ||
                    Convert.ToString(x.Codigo).Contains(vm.Pesquisa.ToUpper()));
                    ListBebidasDescricao.ItemsSource = vm.Bebidas.Where(x => x.Descricao.ToUpper().Contains(vm.Pesquisa.ToUpper()) ||
                    Convert.ToString(x.Codigo).Contains(vm.Pesquisa.ToUpper()));
                }
                else
                {
                    ListCardapioDescricao.ItemsSource = vm.Cardapio;
                    ListBebidasDescricao.ItemsSource = vm.Bebidas;
                }
            }
            catch (Exception ex)
            {
                App.Current.MainPage.DisplayAlert("Amigo", "Erro: " + ex.Message, "OK");
            }
        }
    }
}