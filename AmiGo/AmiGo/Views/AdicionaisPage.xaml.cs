﻿using AmiGo.Models;
using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace AmiGo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AdicionaisPage : PopupPage
    {
		public AdicionaisPage (ObservableCollection<PedidoItem> pedidoItens)
		{
            //App.Current.On<Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Unspecified);
            InitializeComponent ();
            BindingContext = new AdicionaisViewModel(pedidoItens);           
        }

        void Handle_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            //Debug.WriteLine("Posição " + e.NewValue + " Selecionada.");
        }

        void Handle_Scrolled(object sender, CarouselView.FormsPlugin.Abstractions.ScrolledEventArgs e)
        {
            //Debug.WriteLine("Scrolled to " + e.NewValue + " percent.");
            //Debug.WriteLine("Direction = " + e.Direction);
        }
    }
}