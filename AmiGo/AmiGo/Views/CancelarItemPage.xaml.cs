﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using AmiGo.Models;

namespace AmiGo.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CancelarItemPage : PopupPage
    {
        public CancelarItemPage(PedidoItem pedidoItem)
        {
            InitializeComponent();
            BindingContext = new CancelarItemViewModel(pedidoItem);
        }
    }
}