﻿using AmiGo.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmiGo.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ParametroPage : PopupPage
    {
		public ParametroPage ()
		{
			InitializeComponent ();
            BindingContext = new ParametroViewModel();
        }
	}
}